--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: pc_chartofloat(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION pc_chartofloat(chartoconvert character varying) RETURNS double precision
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
SELECT CASE WHEN trim($1) SIMILAR TO '[0-9.]+' 
        THEN CAST(trim($1) AS float) 
    ELSE NULL END;

$_$;


ALTER FUNCTION public.pc_chartofloat(chartoconvert character varying) OWNER TO postgres;

--
-- Name: backups_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE backups_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.backups_seq OWNER TO quickaid;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: backups; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE backups (
    id integer DEFAULT nextval('backups_seq'::regclass) NOT NULL,
    message text,
    hostname character varying(40),
    report_time timestamp without time zone,
    source character varying(64),
    email character varying(255),
    status integer NOT NULL,
    drive_letter character varying(1)
);


ALTER TABLE public.backups OWNER TO quickaid;

--
-- Name: check_result_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE check_result_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.check_result_seq OWNER TO quickaid;

--
-- Name: check_result; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE check_result (
    id integer DEFAULT nextval('check_result_seq'::regclass) NOT NULL,
    description character varying(64) NOT NULL
);


ALTER TABLE public.check_result OWNER TO quickaid;

--
-- Name: checklist_checks_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE checklist_checks_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.checklist_checks_seq OWNER TO quickaid;

--
-- Name: checklist_checks; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE checklist_checks (
    id integer DEFAULT nextval('checklist_checks_seq'::regclass) NOT NULL,
    active boolean DEFAULT true NOT NULL,
    description text NOT NULL,
    item_type integer NOT NULL,
    list_position integer,
    success_description text NOT NULL,
    failure_description text NOT NULL,
    special character varying(32)
);


ALTER TABLE public.checklist_checks OWNER TO quickaid;

--
-- Name: checklist_results_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE checklist_results_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.checklist_results_seq OWNER TO quickaid;

--
-- Name: checklist_results; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE checklist_results (
    id integer DEFAULT nextval('checklist_results_seq'::regclass) NOT NULL,
    sr_id integer NOT NULL,
    check_type_id integer,
    check_result integer,
    submit_time timestamp without time zone,
    technician character varying(64),
    update_time timestamp without time zone,
    version integer,
    comments text
);


ALTER TABLE public.checklist_results OWNER TO quickaid;

--
-- Name: consumable_colours_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE consumable_colours_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.consumable_colours_seq OWNER TO quickaid;

--
-- Name: consumable_colours; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE consumable_colours (
    id integer DEFAULT nextval('consumable_colours_seq'::regclass) NOT NULL,
    description character varying(32) NOT NULL
);


ALTER TABLE public.consumable_colours OWNER TO quickaid;

--
-- Name: consumable_types_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE consumable_types_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.consumable_types_seq OWNER TO quickaid;

--
-- Name: consumable_types; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE consumable_types (
    id integer DEFAULT nextval('consumable_types_seq'::regclass) NOT NULL,
    description character varying(36) NOT NULL
);


ALTER TABLE public.consumable_types OWNER TO quickaid;

--
-- Name: cust_values; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE cust_values (
    list_name character varying(64) DEFAULT ''::character varying NOT NULL,
    value_key integer DEFAULT 0 NOT NULL,
    value_caption character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.cust_values OWNER TO quickaid;

--
-- Name: customer_parts_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE customer_parts_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customer_parts_seq OWNER TO quickaid;

--
-- Name: customer_parts; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE customer_parts (
    id integer DEFAULT nextval('customer_parts_seq'::regclass) NOT NULL,
    part_id integer NOT NULL,
    customer_id character varying(64) NOT NULL
);


ALTER TABLE public.customer_parts OWNER TO quickaid;

--
-- Name: fixed_rates_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE fixed_rates_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fixed_rates_seq OWNER TO quickaid;

--
-- Name: fixed_rates; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE fixed_rates (
    id integer DEFAULT nextval('fixed_rates_seq'::regclass) NOT NULL,
    description character varying(128) NOT NULL,
    organisation character varying(64) NOT NULL,
    rate double precision NOT NULL,
    hours_allowed double precision NOT NULL
);


ALTER TABLE public.fixed_rates OWNER TO quickaid;

--
-- Name: item_types_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE item_types_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.item_types_seq OWNER TO quickaid;

--
-- Name: item_types; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE item_types (
    id integer DEFAULT nextval('item_types_seq'::regclass) NOT NULL,
    description character varying(64) NOT NULL,
    list_position integer NOT NULL
);


ALTER TABLE public.item_types OWNER TO quickaid;

--
-- Name: locations_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE locations_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.locations_seq OWNER TO quickaid;

--
-- Name: locations; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE locations (
    id integer DEFAULT nextval('locations_seq'::regclass) NOT NULL,
    description character varying(36) NOT NULL
);


ALTER TABLE public.locations OWNER TO quickaid;

--
-- Name: meta; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE meta (
    id integer NOT NULL,
    score double precision DEFAULT (0)::double precision NOT NULL,
    duration integer DEFAULT 0 NOT NULL,
    billable integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.meta OWNER TO quickaid;

--
-- Name: notes_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE notes_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notes_seq OWNER TO quickaid;

--
-- Name: notes; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE notes (
    id integer DEFAULT nextval('notes_seq'::regclass) NOT NULL,
    sr_id integer NOT NULL,
    user_name character varying(64) NOT NULL,
    type integer NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    description text,
    attachment_type character varying(12) DEFAULT NULL::character varying,
    filename character varying(255)
);


ALTER TABLE public.notes OWNER TO quickaid;

--
-- Name: part_types_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE part_types_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.part_types_seq OWNER TO quickaid;

--
-- Name: part_types; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE part_types (
    id integer DEFAULT nextval('part_types_seq'::regclass) NOT NULL,
    description character varying(64) NOT NULL
);


ALTER TABLE public.part_types OWNER TO quickaid;

--
-- Name: parts_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE parts_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.parts_seq OWNER TO quickaid;

--
-- Name: parts; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE parts (
    id integer DEFAULT nextval('parts_seq'::regclass) NOT NULL,
    part_type integer NOT NULL,
    vendor integer NOT NULL,
    model character varying(128),
    part_number character varying(24),
    soh_desired integer NOT NULL,
    soh_current integer NOT NULL,
    barcode character varying(32),
    discontinued boolean DEFAULT false NOT NULL,
    notes text,
    location character varying(16)
);


ALTER TABLE public.parts OWNER TO quickaid;

--
-- Name: parts_properties_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE parts_properties_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.parts_properties_seq OWNER TO quickaid;

--
-- Name: parts_properties; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE parts_properties (
    id integer DEFAULT nextval('parts_properties_seq'::regclass) NOT NULL,
    part_id integer NOT NULL,
    name character varying(128),
    value text
);


ALTER TABLE public.parts_properties OWNER TO quickaid;

--
-- Name: printer_consumables_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE printer_consumables_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.printer_consumables_seq OWNER TO quickaid;

--
-- Name: printer_consumables; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE printer_consumables (
    id integer DEFAULT nextval('printer_consumables_seq'::regclass) NOT NULL,
    printer_id integer NOT NULL,
    consumable_id integer NOT NULL
);


ALTER TABLE public.printer_consumables OWNER TO quickaid;

--
-- Name: printer_types_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE printer_types_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.printer_types_seq OWNER TO quickaid;

--
-- Name: printer_types; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE printer_types (
    id integer DEFAULT nextval('printer_types_seq'::regclass) NOT NULL,
    description character varying(36) NOT NULL
);


ALTER TABLE public.printer_types OWNER TO quickaid;

--
-- Name: service_req_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE service_req_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_req_seq OWNER TO quickaid;

--
-- Name: service_req; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE service_req (
    id integer DEFAULT nextval('service_req_seq'::regclass) NOT NULL,
    problem_type character varying(64) DEFAULT NULL::character varying,
    title character varying(255) DEFAULT NULL::character varying,
    description text,
    status integer,
    contact text,
    responsibility character varying(64) DEFAULT NULL::character varying,
    urgency integer,
    notes text,
    resolution text,
    solution text,
    insert_time timestamp without time zone,
    update_time timestamp without time zone,
    close_time timestamp without time zone,
    update_user character varying(64) DEFAULT NULL::character varying,
    submit_user character varying(64) DEFAULT NULL::character varying,
    request_user character varying(64) DEFAULT NULL::character varying,
    due_date timestamp without time zone,
    cust_text1 character varying(255) DEFAULT NULL::character varying,
    cust_text2 character varying(255) DEFAULT NULL::character varying,
    cust_notes text,
    cust_int1 integer,
    cust_int2 integer,
    full_name character varying(255),
    contact_email character varying(80),
    fixed_rate integer,
    invoice_time timestamp without time zone,
    quote_no character varying(32),
    order_status integer DEFAULT 0,
    quote_value numeric(10,2),
    acceptance_time timestamp without time zone,
    quote_winner character varying(64)
);


ALTER TABLE public.service_req OWNER TO quickaid;

--
-- Name: timesheets_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE timesheets_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.timesheets_seq OWNER TO quickaid;

--
-- Name: timesheets; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE timesheets (
    id integer DEFAULT nextval('timesheets_seq'::regclass) NOT NULL,
    timesheet_date timestamp without time zone NOT NULL,
    submit_date timestamp without time zone NOT NULL,
    user_name character varying(64) NOT NULL
);


ALTER TABLE public.timesheets OWNER TO quickaid;

--
-- Name: users_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE users_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_seq OWNER TO quickaid;

--
-- Name: users; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE users (
    user_id integer DEFAULT nextval('users_seq'::regclass) NOT NULL,
    user_name character varying(32) NOT NULL,
    first_name character varying(32) NOT NULL,
    last_name character varying(32) NOT NULL,
    email_addr character varying(64) DEFAULT NULL::character varying,
    phone_no character varying(20) DEFAULT NULL::character varying,
    address text,
    administrator smallint DEFAULT (0)::smallint NOT NULL,
    organisation_name character varying(64) DEFAULT NULL::character varying,
    active smallint DEFAULT (1)::smallint NOT NULL,
    ldap smallint DEFAULT (0)::smallint NOT NULL,
    password character varying(128) DEFAULT NULL::character varying,
    technician integer,
    failed_logins numeric DEFAULT 0 NOT NULL,
    last_failed_login timestamp without time zone
);


ALTER TABLE public.users OWNER TO quickaid;

--
-- Name: vendors_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE vendors_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vendors_seq OWNER TO quickaid;

--
-- Name: vendors; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE vendors (
    id integer DEFAULT nextval('vendors_seq'::regclass) NOT NULL,
    description character varying(36) NOT NULL
);


ALTER TABLE public.vendors OWNER TO quickaid;

--
-- Name: work_report_seq; Type: SEQUENCE; Schema: public; Owner: quickaid
--

CREATE SEQUENCE work_report_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.work_report_seq OWNER TO quickaid;

--
-- Name: work_report; Type: TABLE; Schema: public; Owner: quickaid; Tablespace: 
--

CREATE TABLE work_report (
    id integer DEFAULT nextval('work_report_seq'::regclass) NOT NULL,
    service_req_id integer,
    user_name character varying(64) DEFAULT NULL::character varying,
    from_time timestamp without time zone,
    to_time timestamp without time zone,
    description text,
    cust_list1 integer,
    billable_hours double precision DEFAULT 0,
    cust_text2 character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.work_report OWNER TO quickaid;

--
-- Name: work_report_temp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE work_report_temp (
    id integer,
    service_req_id integer,
    user_name character varying(64),
    from_time timestamp without time zone,
    to_time timestamp without time zone,
    description text,
    cust_list1 integer,
    cust_text1 double precision DEFAULT 0,
    cust_text2 character varying(255)
);


ALTER TABLE public.work_report_temp OWNER TO postgres;

--
-- Name: backups_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY backups
    ADD CONSTRAINT backups_pkey PRIMARY KEY (id);


--
-- Name: check_result_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY check_result
    ADD CONSTRAINT check_result_pkey PRIMARY KEY (id);


--
-- Name: checklist_checks_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY checklist_checks
    ADD CONSTRAINT checklist_checks_pkey PRIMARY KEY (id);


--
-- Name: checklist_results_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY checklist_results
    ADD CONSTRAINT checklist_results_pkey PRIMARY KEY (id);


--
-- Name: consumable_colours_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY consumable_colours
    ADD CONSTRAINT consumable_colours_pkey PRIMARY KEY (id);


--
-- Name: consumable_types_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY consumable_types
    ADD CONSTRAINT consumable_types_pkey PRIMARY KEY (id);


--
-- Name: cust_values_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY cust_values
    ADD CONSTRAINT cust_values_pkey PRIMARY KEY (list_name, value_key);


--
-- Name: customer_parts_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY customer_parts
    ADD CONSTRAINT customer_parts_pkey PRIMARY KEY (id);


--
-- Name: fixed_rates_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY fixed_rates
    ADD CONSTRAINT fixed_rates_pkey PRIMARY KEY (id);


--
-- Name: item_types_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY item_types
    ADD CONSTRAINT item_types_pkey PRIMARY KEY (id);


--
-- Name: locations_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);


--
-- Name: meta_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY meta
    ADD CONSTRAINT meta_pkey PRIMARY KEY (id);


--
-- Name: notes_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY notes
    ADD CONSTRAINT notes_pkey PRIMARY KEY (id);


--
-- Name: part_types_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY part_types
    ADD CONSTRAINT part_types_pkey PRIMARY KEY (id);


--
-- Name: parts_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY parts
    ADD CONSTRAINT parts_pkey PRIMARY KEY (id);


--
-- Name: parts_properties_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY parts_properties
    ADD CONSTRAINT parts_properties_pkey PRIMARY KEY (id);


--
-- Name: printer_consumables_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY printer_consumables
    ADD CONSTRAINT printer_consumables_pkey PRIMARY KEY (id);


--
-- Name: printer_types_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY printer_types
    ADD CONSTRAINT printer_types_pkey PRIMARY KEY (id);


--
-- Name: service_req_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY service_req
    ADD CONSTRAINT service_req_pkey PRIMARY KEY (id);


--
-- Name: timesheets_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY timesheets
    ADD CONSTRAINT timesheets_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: vendors_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY vendors
    ADD CONSTRAINT vendors_pkey PRIMARY KEY (id);


--
-- Name: work_report_pkey; Type: CONSTRAINT; Schema: public; Owner: quickaid; Tablespace: 
--

ALTER TABLE ONLY work_report
    ADD CONSTRAINT work_report_pkey PRIMARY KEY (id);


--
-- Name: checklist_checks_item_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: quickaid
--

ALTER TABLE ONLY checklist_checks
    ADD CONSTRAINT checklist_checks_item_type_fkey FOREIGN KEY (item_type) REFERENCES item_types(id);


--
-- Name: checklist_results_check_result_fkey; Type: FK CONSTRAINT; Schema: public; Owner: quickaid
--

ALTER TABLE ONLY checklist_results
    ADD CONSTRAINT checklist_results_check_result_fkey FOREIGN KEY (check_result) REFERENCES check_result(id);


--
-- Name: checklist_results_check_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: quickaid
--

ALTER TABLE ONLY checklist_results
    ADD CONSTRAINT checklist_results_check_type_id_fkey FOREIGN KEY (check_type_id) REFERENCES checklist_checks(id);


--
-- Name: checklist_results_sr_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: quickaid
--

ALTER TABLE ONLY checklist_results
    ADD CONSTRAINT checklist_results_sr_id_fkey FOREIGN KEY (sr_id) REFERENCES service_req(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: fixed_rates; Type: ACL; Schema: public; Owner: quickaid
--

REVOKE ALL ON TABLE fixed_rates FROM PUBLIC;
REVOKE ALL ON TABLE fixed_rates FROM quickaid;
GRANT ALL ON TABLE fixed_rates TO quickaid;


--
-- Name: meta; Type: ACL; Schema: public; Owner: quickaid
--

REVOKE ALL ON TABLE meta FROM PUBLIC;
REVOKE ALL ON TABLE meta FROM quickaid;
GRANT ALL ON TABLE meta TO quickaid;


--
-- PostgreSQL database dump complete
--


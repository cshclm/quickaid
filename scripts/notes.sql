Creating quickaid_notes:

create table quickaid_notes (id INT NOT NULL AUTO_INCREMENT, sr_id INT NOT NULL, user_name varchar(64) not null, primary key(id), type int not null, timestamp datetime not null, description text, foreign key (sr_id) references service_req(id));

create table users (user_id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(user_id), user_name VARCHAR(32) NOT NULL UNIQUE, first_name VARCHAR(32) NOT NULL, last_name VARCHAR(32) NOT NULL, email_addr VARCHAR(64), phone_no VARCHAR(20), address TEXT, administrator boolean not null default 0, organisation_name VARCHAR(64));


user_name
first_name
last_name
email_address
phone
administrator
organisation_id = 0

DROP TABLE IF EXISTS part_types;
DROP SEQUENCE IF EXISTS part_types_seq;

CREATE SEQUENCE part_types_seq;
CREATE TABLE part_types (
       id INT NOT NULL DEFAULT nextval('part_types_seq'),
       description varchar(64) NOT NULL,
       primary key(id)
);

DROP TABLE IF EXISTS printerconsumable;
DROP SEQUENCE IF EXISTS printerconsumable_seq;
DROP TABLE IF EXISTS printer_consumables;
DROP SEQUENCE IF EXISTS printer_consumables_seq;

CREATE SEQUENCE printer_consumables_seq;
CREATE TABLE printer_consumables (
       id INT NOT NULL DEFAULT nextval('printer_consumables_seq'),
       printer_id INT NOT NULL,
       consumable_id INT NOT NULL,
       primary key(id)
);

DROP TABLE IF EXISTS parts;
DROP SEQUENCE IF EXISTS parts_seq;

CREATE SEQUENCE parts_seq;
CREATE TABLE parts (
       id INT NOT NULL DEFAULT nextval('parts_seq'),
       part_type INT NOT NULL,
       vendor INT NOT NULL,
       model varchar(128),
       part_number varchar(24),
       soh_desired INT NOT NULL,
       soh_current INT NOT NULL,
       barcode varchar(32),
       discontinued boolean NOT NULL DEFAULT FALSE,
       notes text,
       location varchar(16),
       primary key(id)
);


DROP TABLE IF EXISTS parts_properties;
DROP SEQUENCE IF EXISTS parts_properties_seq;
CREATE SEQUENCE parts_properties_seq;
CREATE TABLE parts_properties (
       id INT NOT NULL DEFAULT nextval('parts_properties_seq'),
       part_id INT NOT NULL,
       name varchar(128),
       value text,
       primary key(id)
);


DROP TABLE IF EXISTS customer_parts;
DROP SEQUENCE IF EXISTS customer_parts_seq;
DROP TABLE IF EXISTS customerprinters;
DROP SEQUENCE IF EXISTS customerprinters_seq;

CREATE SEQUENCE customer_parts_seq;
CREATE TABLE customer_parts (
       id INT NOT NULL DEFAULT nextval('customer_parts_seq'),
       part_id INT NOT NULL,
       customer_id varchar(64) NOT NULL,
       primary key(id)
);

DROP TABLE IF EXISTS consumable_colours;
DROP SEQUENCE IF EXISTS consumable_colours_seq;

CREATE SEQUENCE consumable_colours_seq;
CREATE TABLE consumable_colours (
       id INT NOT NULL DEFAULT nextval('consumable_colours_seq'),
       description varchar(32) NOT NULL,
       primary key(id)
);

DROP TABLE IF EXISTS locations;
DROP SEQUENCE IF EXISTS locations_seq;
DROP TABLE IF EXISTS location;
DROP SEQUENCE IF EXISTS location_seq;

CREATE SEQUENCE locations_seq;
CREATE TABLE locations (
       id INT NOT NULL DEFAULT nextval('locations_seq'),
       description varchar(36) NOT NULL,
       primary key(id)
);

DROP TABLE IF EXISTS vendors;
DROP SEQUENCE IF EXISTS vendors_seq;

CREATE SEQUENCE vendors_seq;
CREATE TABLE vendors (
       id INT NOT NULL DEFAULT nextval('vendors_seq'),
       description varchar(36) NOT NULL,
       primary key(id)
);


DROP TABLE IF EXISTS printer_types;
DROP SEQUENCE IF EXISTS printer_types_seq;

CREATE SEQUENCE printer_types_seq;
CREATE TABLE printer_types (
       id INT NOT NULL DEFAULT nextval('printer_types_seq'),
       description varchar(36) NOT NULL,
       primary key(id)
);


DROP TABLE IF EXISTS printer_types;
DROP SEQUENCE IF EXISTS printer_types_seq;

CREATE SEQUENCE printer_types_seq;
CREATE TABLE printer_types (
       id INT NOT NULL DEFAULT nextval('printer_types_seq'),
       description varchar(36) NOT NULL,
       primary key(id)
);


DROP TABLE IF EXISTS consumable_types;
DROP SEQUENCE IF EXISTS consumable_types_seq;

CREATE SEQUENCE consumable_types_seq;
CREATE TABLE consumable_types (
       id INT NOT NULL DEFAULT nextval('consumable_types_seq'),
       description varchar(36) NOT NULL,
       primary key(id)
);


INSERT INTO part_types (description) VALUES ('Printer consumable');
INSERT INTO part_types (description) VALUES ('Printer');
INSERT INTO part_types (description) VALUES ('Other');


INSERT INTO consumable_types (description) VALUES ('Toner');
INSERT INTO consumable_types (description) VALUES ('Ink');
INSERT INTO consumable_types (description) VALUES ('Printhead');
INSERT INTO consumable_types (description) VALUES ('Other');


INSERT INTO printer_types (description) VALUES ('Laser Mono');
INSERT INTO printer_types (description) VALUES ('Laser Colour');
INSERT INTO printer_types (description) VALUES ('Ink Colour');
INSERT INTO printer_types (description) VALUES ('Plotter Colour');
INSERT INTO printer_types (description) VALUES ('Plotter Mono');
INSERT INTO printer_types (description) VALUES ('Dot Matrix');
INSERT INTO printer_types (description) VALUES ('Ink Mono');
INSERT INTO printer_types (description) VALUES ('Thermal');


INSERT INTO consumable_colours (description) VALUES ('Black');
INSERT INTO consumable_colours (description) VALUES ('Cyan');
INSERT INTO consumable_colours (description) VALUES ('Magenta');
INSERT INTO consumable_colours (description) VALUES ('Yellow');
INSERT INTO consumable_colours (description) VALUES ('Tri-colour');
INSERT INTO consumable_colours (description) VALUES ('Grey');
INSERT INTO consumable_colours (description) VALUES ('Twin Colour');
INSERT INTO consumable_colours (description) VALUES ('Combo Pack');


INSERT INTO vendors (description) VALUES ('APC-Schneider');
INSERT INTO vendors (description) VALUES ('Apple');
INSERT INTO vendors (description) VALUES ('ATEN');
INSERT INTO vendors (description) VALUES ('Belkin');
INSERT INTO vendors (description) VALUES ('Bliss');
INSERT INTO vendors (description) VALUES ('Board Smart');
INSERT INTO vendors (description) VALUES ('BOSE');
INSERT INTO vendors (description) VALUES ('Brother');
INSERT INTO vendors (description) VALUES ('Canon');
INSERT INTO vendors (description) VALUES ('Cisco');
INSERT INTO vendors (description) VALUES ('Cisco Linksys');
INSERT INTO vendors (description) VALUES ('Dell');
INSERT INTO vendors (description) VALUES ('Deppcool');
INSERT INTO vendors (description) VALUES ('D-Link');
INSERT INTO vendors (description) VALUES ('ECCO Pacific');
INSERT INTO vendors (description) VALUES ('Epson');
INSERT INTO vendors (description) VALUES ('Fuji-Xerox');
INSERT INTO vendors (description) VALUES ('Generic');
INSERT INTO vendors (description) VALUES ('HP');
INSERT INTO vendors (description) VALUES ('iLuv');
INSERT INTO vendors (description) VALUES ('Intel');
INSERT INTO vendors (description) VALUES ('Iomega');
INSERT INTO vendors (description) VALUES ('IRIS');
INSERT INTO vendors (description) VALUES ('Jackson');
INSERT INTO vendors (description) VALUES ('Konica Minolta');
INSERT INTO vendors (description) VALUES ('Kouwell');
INSERT INTO vendors (description) VALUES ('Kyocera');
INSERT INTO vendors (description) VALUES ('Lacie');
INSERT INTO vendors (description) VALUES ('Lexmark');
INSERT INTO vendors (description) VALUES ('Logitech');
INSERT INTO vendors (description) VALUES ('Microsoft');
INSERT INTO vendors (description) VALUES ('Netcomm');
INSERT INTO vendors (description) VALUES ('Netgear');
INSERT INTO vendors (description) VALUES ('Samsung');
INSERT INTO vendors (description) VALUES ('Seagate');
INSERT INTO vendors (description) VALUES ('Sunix');
INSERT INTO vendors (description) VALUES ('Targus');
INSERT INTO vendors (description) VALUES ('Toshiba');
INSERT INTO vendors (description) VALUES ('Trend Micro');
INSERT INTO vendors (description) VALUES ('V7');
INSERT INTO vendors (description) VALUES ('Zebra');
INSERT INTO vendors (description) VALUES ('Eaton');

INSERT INTO locations (description) VALUES ('A1');
INSERT INTO locations (description) VALUES ('A2');
INSERT INTO locations (description) VALUES ('A3');
INSERT INTO locations (description) VALUES ('A4');
INSERT INTO locations (description) VALUES ('A5');
INSERT INTO locations (description) VALUES ('A6');
INSERT INTO locations (description) VALUES ('B1');
INSERT INTO locations (description) VALUES ('B2');
INSERT INTO locations (description) VALUES ('B4');
INSERT INTO locations (description) VALUES ('B5');
INSERT INTO locations (description) VALUES ('B6');
INSERT INTO locations (description) VALUES ('C1');
INSERT INTO locations (description) VALUES ('C2');
INSERT INTO locations (description) VALUES ('C3');
INSERT INTO locations (description) VALUES ('C4');
INSERT INTO locations (description) VALUES ('C5');
INSERT INTO locations (description) VALUES ('C6');
INSERT INTO locations (description) VALUES ('D1');
INSERT INTO locations (description) VALUES ('D2');
INSERT INTO locations (description) VALUES ('D3');
INSERT INTO locations (description) VALUES ('D4');
INSERT INTO locations (description) VALUES ('D5');
INSERT INTO locations (description) VALUES ('D6');
INSERT INTO locations (description) VALUES ('E1');
INSERT INTO locations (description) VALUES ('E2');
INSERT INTO locations (description) VALUES ('E3');
INSERT INTO locations (description) VALUES ('E4');
INSERT INTO locations (description) VALUES ('E5');
INSERT INTO locations (description) VALUES ('E6');
INSERT INTO locations (description) VALUES ('F1');
INSERT INTO locations (description) VALUES ('F2');
INSERT INTO locations (description) VALUES ('F3');
INSERT INTO locations (description) VALUES ('F4');
INSERT INTO locations (description) VALUES ('F5');
INSERT INTO locations (description) VALUES ('F6');
INSERT INTO locations (description) VALUES ('G1');
INSERT INTO locations (description) VALUES ('G2');
INSERT INTO locations (description) VALUES ('G3');
INSERT INTO locations (description) VALUES ('G4');
INSERT INTO locations (description) VALUES ('G5');
INSERT INTO locations (description) VALUES ('G6');
INSERT INTO locations (description) VALUES ('H1');
INSERT INTO locations (description) VALUES ('H2');
INSERT INTO locations (description) VALUES ('H3');
INSERT INTO locations (description) VALUES ('H4');
INSERT INTO locations (description) VALUES ('H5');
INSERT INTO locations (description) VALUES ('H6');
INSERT INTO locations (description) VALUES ('I1');
INSERT INTO locations (description) VALUES ('I2');
INSERT INTO locations (description) VALUES ('I3');
INSERT INTO locations (description) VALUES ('I4');
INSERT INTO locations (description) VALUES ('I5');
INSERT INTO locations (description) VALUES ('I6');
INSERT INTO locations (description) VALUES ('J1');
INSERT INTO locations (description) VALUES ('J2');
INSERT INTO locations (description) VALUES ('J3');
INSERT INTO locations (description) VALUES ('J4');
INSERT INTO locations (description) VALUES ('J5');
INSERT INTO locations (description) VALUES ('J6');
INSERT INTO locations (description) VALUES ('K1');
INSERT INTO locations (description) VALUES ('K2');
INSERT INTO locations (description) VALUES ('K3');
INSERT INTO locations (description) VALUES ('K4');
INSERT INTO locations (description) VALUES ('K5');
INSERT INTO locations (description) VALUES ('K6');
INSERT INTO locations (description) VALUES ('L1');
INSERT INTO locations (description) VALUES ('L2');
INSERT INTO locations (description) VALUES ('L3');
INSERT INTO locations (description) VALUES ('L4');
INSERT INTO locations (description) VALUES ('L5');
INSERT INTO locations (description) VALUES ('L6');
INSERT INTO locations (description) VALUES ('M1');
INSERT INTO locations (description) VALUES ('M2');
INSERT INTO locations (description) VALUES ('M3');
INSERT INTO locations (description) VALUES ('M4');
INSERT INTO locations (description) VALUES ('M5');
INSERT INTO locations (description) VALUES ('M6');

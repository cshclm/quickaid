#!/usr/bin/perl
use strict;
use warnings;

use DBI;
use DBD::mysql;
use SQL::Library;
use Text::CSV;

BEGIN { require($ENV{PWD} . '/../lib/QuickAid/config.pl'); }

my $sql = new SQL::Library {
  lib => ( $ENV{PWD} . '/integrate-sugarcrm.lib'),
};

my $dbh = DBI->connect('DBI:' . &user_config('sql','type') .
                       ':' . &user_config('sql','name') .
                       ';host=' . &user_config('sql','host') .
                       ';port=' . &user_config('sql','port'),
                       &user_config('sql','user'), &user_config('sql','passwd'),
                       { RaiseError => 1, AutoCommit => 1 })
  or die "Could not connect to database: $DBI::errstr";


$dbh->{'mysql_enable_utf8'} = 1;
$dbh->do(qq{SET NAMES 'utf8' COLLATE utf8_general_ci;});

my $csv = Text::CSV->new ( { binary => 1 } )  # should set binary attribute.
               or die "Cannot use CSV: ".Text::CSV->error_diag ();
open my $fh, "<:encoding(utf8)", "conversion.csv" or die "$!";
while ( my $row = $csv->getline( $fh ) ) {
  next if $row->[0] =~ "Customer";
  if ($row->[1]) {
    update_customer($row->[1], $row->[0]);
  } else {
    update_customer("New Customer", $row->[0]);
  }
}

sub update_customer {
  my ($customer,$id) = @_;
  my $sth = $dbh->prepare($sql->retr('update_service_req'));
  $sth->execute($customer,$id);
  return 1;
}

sub db_disconnect {
  $dbh->disconnect;
}

1;

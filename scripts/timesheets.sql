DROP TABLE IF EXISTS timesheets;
DROP SEQUENCE IF EXISTS timesheets_seq;

CREATE SEQUENCE timesheets_seq;
CREATE TABLE timesheets (
       id INT NOT NULL DEFAULT nextval('timesheets_seq'),
       timesheet_date timestamp NOT NULL,
       submit_date timestamp NOT NULL,
       user_name varchar(64) NOT NULL,
       primary key(id)
);

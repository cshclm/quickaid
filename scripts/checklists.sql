CREATE SEQUENCE item_types_seq;
CREATE TABLE item_types (
       id INT NOT NULL DEFAULT nextval('item_types_seq'),
       description varchar(64) NOT NULL,
       list_position INT NOT NULL,
       primary key(id)
);

CREATE SEQUENCE check_result_seq;
CREATE TABLE check_result (
       id INT NOT NULL DEFAULT nextval('check_result_seq'),
       description varchar(64) NOT NULL,
       primary key(id)
);

CREATE SEQUENCE checklist_checks_seq;
CREATE TABLE checklist_checks (
       id INT NOT NULL DEFAULT nextval('checklist_checks_seq'),
       active BOOLEAN NOT NULL DEFAULT true,
       description TEXT NOT NULL,
       item_type INT NOT NULL references item_types(id),
       list_position INT,
       primary key(id)
);

CREATE SEQUENCE checklist_results_seq;
CREATE TABLE checklist_results (
       id INT NOT NULL DEFAULT nextval('checklist_results_seq'),
       sr_id INT NOT NULL REFERENCES service_req(id),
       customer_id VARCHAR(64) NOT NULL,
       check_type_id INT references checklist_checks(id),
       check_result INT references check_result(id),
       submit_time timestamp without time zone,
       technician VARCHAR(64),
       primary key(id)
);

INSERT INTO check_result (description) VALUES ('Complete');
INSERT INTO check_result (description) VALUES ('Follow-up needed');
INSERT INTO check_result (description) VALUES ('Not Applicable');

INSERT INTO item_types (description, list_position) VALUES ('Site', 1);
INSERT INTO item_types (description, list_position) VALUES ('Server Hardware', 5);
INSERT INTO item_types (description, list_position) VALUES ('Backups', 10);
INSERT INTO item_types (description, list_position) VALUES ('Anti-Virus', 15);
INSERT INTO item_types (description, list_position) VALUES ('UPS', 20);
INSERT INTO item_types (description, list_position) VALUES ('Server Software', 25);
INSERT INTO item_types (description, list_position) VALUES ('Storage', 30);
INSERT INTO item_types (description, list_position) VALUES ('Workstation', 35);
INSERT INTO item_types (description, list_position) VALUES ('Printing', 40);
INSERT INTO item_types (description, list_position) VALUES ('Focus Task', 70); 
INSERT INTO item_types (description, list_position) VALUES ('Documentation', 90);
INSERT INTO item_types (description, list_position) VALUES ('Finalisation', 95);

INSERT INTO checklist_checks (description, item_type, list_position) VALUES
       ('Task 1', 1, 5);
INSERT INTO checklist_checks (description, item_type, list_position) VALUES
       ('Task 2', 3, 5);

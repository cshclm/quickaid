#!/usr/bin/env perl
use strict;
use warnings;

use lib 'lib';
use QuickAid;
use QuickAid::DB qw/query_db/;
use QuickAid::Config::SiteConfig qw/site_config/;
use QuickAid::Config::UserConfig qw/user_config/;
use File::Copy qw(move);
use MIME::Base64;
use Try::Tiny;

sub process_files {
  my $dir = &site_config('mailbox', 'inbox_dir');

  opendir(my $dh, $dir);
  while (defined(my $file = readdir $dh)) {
    next unless -f "$dir/$file";
    parse_file("$file");
  }
  closedir($dh);
}

sub parse_file {
  my $file = shift;
  my $dir = &site_config('mailbox', 'inbox_dir');
  my $archivedir = &site_config('mailbox', 'archive_dir');

  open(my $fh, "< $dir/$file");
  my ($field, $value, $oldfield);
  my %details;
  while (defined (my $line = <$fh>)) {
    ($field, $value) = extract_info($line);

    if (!defined($value)) {
      ($oldfield, $field) = ('Body', 'Body');
    }
    elsif (defined($field)) {
      $oldfield = $field;
    } else {
      $field = $oldfield;
    }
    if (defined($value) and $value =~ /(?<value>.+)$/) {
      $details{$field} .= $+{value};
    } else {
      if ($value) {
        $details{$field} .= $value;
        $details{$field} .= "\n" if ($field eq 'Body');
      }
    }
  }
  close($fh);

  if (process_results(\%details)) {
    move ("$dir/$file", "$archivedir/$file") or warn "$!";
  }
}

sub extract_info {
  my $line = shift;
  return undef if ($line =~ /^$/);
  return ($+{field}, $+{value}) if ($line =~ /^(?<field>[^ ]+): (?<value>.+)$/);
  return (undef, $+{value}) if ($line =~ /^(?<value>.+)$/);
}


sub process_results {
  my $details = shift;
  my $months = &user_config('misc', 'months');
  if (defined($details->{'Subject'})) {
    $details->{Date} =~ /(?[0-9]+)\s
                         (?[A-Za-z]+)\s
                         (?[0-9]+)\s
                         (?[0-9]+):
                         (?[0-9]+):
                         (?[0-9]+)\s/x;
    $details->{Date} = "$+{year}-$months->{$+{month}}-$+{day} $+{hour}:$+{min}:$+{sec}";
    $details->{From} = $+{email}
      if ($details->{From} =~ /^\.+)\>$/);

    if ($details->{'Subject'} =~ /^ShadowProtect (backup|job) (?failure|success) on (?[^\s]+)/) {
      my $result = $+{result};
      $details->{'Host'} = $+{host};
      $details->{From} = $+{email}
        if ($details->{From} =~ /^\.+)\>$/);

      # Strip trailing '=\n' indicating line wrap
      $details->{Body} =~ s/=\n//g;

      query_db('noret', 'new_backup_result',
               $details->{Host},
               $details->{Date},
               $details->{From},
               (($result =~ /success/) ? 1 : 2),
               $details->{Body});
      return 1;
    } elsif ($details->{'Subject'} =~ /=\?iso-8859-1\?(?.+?)\?=/) {
      return 0 if $details->{Subject} =~ /ImageManager/;
      my $subject = $+{subject};
      $subject =~ /^ShadowProtect (backup|job) (?failure|success) on (?[^\s]+)/;
      $details->{'Host'} = $+{host};
      $details->{From} = $+{email};
   my $result = $+{result};
      $details->{Body} =~ s/=20/ /g;
      chomp($details->{Body});
      query_db('noret', 'new_backup_result',
               $details->{Host},
               $details->{Date},
               $details->{From},
               (($result =~ /success/) ? 1 : 2),
               $details->{Body});
      return 1;
    } elsif ($details->{'Subject'} =~ /=\?utf-8\?B\?(?.+?)\?=/) {
      try {
        $details->{Subject} = decode_base64($+{subject});
        return procses_encoded_im_message($details)
          if $details->{Subject} =~ /ImageManager/;

        $details->{Subject} =~ /^ShadowProtect (backup|job) (?<result>failure|success) on (?<host>[^\s]+)/;
        my $result = $+{result};
        $details->{'Host'} = $+{host};

        chomp($details->{Body});
        $details->{Body} = decode_base64($details->{Body});
        my $problem = $+{problem};
        query_db('noret', 'new_backup_result',
               $details->{Host},
               $details->{Date},
               $details->{From},
               (($result =~ /success/) ? 1 : 2),
               $details->{Body});
      } catch {
        print "Unable to process ImageManager message\n";
        return 0;
      };
      return 1;
    } elsif ($details->{'Subject'} =~ /ImageManager/) {
        $details->{Body} =~ s/=\n//g;
        query_db('noret', 'new_imagemanager_result',
                 $details->{Date},
                 $details->{From},
                 $details->{Subject} . "\n" . $details->{Body});
        return 1;
    } elsif (
 $details->{'Subject'} =~ /^UPS: Compensating for a high input voltage/ or
 $details->{'Subject'} =~ /^UPS: No longer compensating for a high input voltage/ or
 $details->{'Subject'} =~ /^\*\* PROBLEM/
 ) {
 return 1;
    } else {
 print "Unable to process $details->{'Subject'} \n";
    }
  }
  return 0;
}

sub process_encoded_im_message {
  my $details = shift;
  try {
    $details->{Subject} = decode_base64($+{subject});
    chomp($details->{Body});
    $details->{Body} = decode_base64($details->{Body});
    my $problem = $+{problem};
    query_db('noret', 'new_imagemanager_result',
             $details->{Date},
             $details->{From},
             $details->{Subject} . "\n" . $details->{Body});
    return 1;
  } catch {
    return 1;
  }
}

process_files;

1;

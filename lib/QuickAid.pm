package QuickAid;

use Dancer;
use QuickAid::Handler::Core;
use QuickAid::Handler::Case;
use QuickAid::Handler::Backups;
use QuickAid::Handler::Parts;
use QuickAid::Config::UserConfig qw/user_config/;
use QuickAid::Config::SiteConfig qw/site_config/;

set 'session_dir' => $ENV{PWD} . "/sessions";
set layout => 'main.tt';

set dev_instance => 1
  if (defined(&site_config('deployment','development')) and
      &site_config('deployment','development'));

1;

__END__

=pod

=head1 NAME

QuickAid is a helpdesk management and reporting application

=head1 VERSION

This document refers to QuickAid 0.13.3

#!/usr/bin/perl

package QuickAid;

use strict;
use warnings;
use Dancer;
use lib ($ENV{PWD} . '/lib');

use QuickAid::Handler::Case qw/update_metadata_all_cases/;

&QuickAid::Handler::Case::update_metadata_all_cases();

1;

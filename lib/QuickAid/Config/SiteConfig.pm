package QuickAid::Config::SiteConfig;

BEGIN {
  use Exporter ();

  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

  # set the version for version checking
  $VERSION = 0.1;

  @ISA = qw/Exporter/ ;
  @EXPORT = qw($site_config);
  %EXPORT_TAGS = ( );           # eg: TAG => [ qw!name1 name2! ],
  # your exported package globals go here,
  # as well as any optionally exported functions
  @EXPORT_OK = qw(
                   &site_config
                );
}
our @EXPORT_OK;

sub site_config {
  my ($section, $key) = @_;
  return $site_config->{$section}->{$key};
}

BEGIN {
  require ($ENV{PWD} . "/lib/QuickAid/site-config.pl");
};

1;

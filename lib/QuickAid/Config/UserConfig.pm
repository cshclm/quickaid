package QuickAid::Config::UserConfig;

BEGIN {
  use Exporter ();

  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

  # set the version for version checking
  $VERSION = 0.1;

  @ISA = qw/Exporter/ ;
  @EXPORT = qw($config);
  %EXPORT_TAGS = ( );           # eg: TAG => [ qw!name1 name2! ],
  # your exported package globals go here,
  # as well as any optionally exported functions
  @EXPORT_OK = qw(
                   &user_config
                );
}
our @EXPORT_OK;

sub user_config {
  my ($section, $key) = @_;
  return $config->{$section}->{$key};
}

BEGIN {
  require ($ENV{PWD} . "/lib/QuickAid/config.pl");
};

1;

package QuickAid::Notifications;

use strict;
use warnings;

use Dancer;
use Dancer::Plugin::Email;
use Try::Tiny;
use Log::Log4perl qw(:easy);

use QuickAid::Config::UserConfig qw/&user_config/;
use QuickAid::Config::SiteConfig qw/&site_config/;

BEGIN {
  use Exporter ();

  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

  # set the version for version checking
  $VERSION = 0.1;

  @ISA = qw/Exporter/ ;
  @EXPORT = qw();
  %EXPORT_TAGS = ( );           # eg: TAG => [ qw!name1 name2! ],
  # your exported package globals go here,
  # as well as any optionally exported functions
  @EXPORT_OK = qw(
                   &notify
                );
}
our @EXPORT_OK;

sub send_email {
  my ($subject, $body, @recips) = @_;
  try {
    email ({
            from => &user_config('email','sender'),
            to => join(", ", @recips),
            subject => $subject,
            body => $body,
           })
      if (@recips);
  } catch {
    ALWAYS "Could not send e-mail: $_";
  };
}

sub notify {
  my $type = shift;
  DEBUG "Preparing to send e-mail of type $type...";
  my %dispatch = (
                  customer => \&notify_customer_case_update,
                  caseupdate => \&notify_case_update,
                  casenote => \&notify_new_case_note,
                  timesheet => \&notify_timesheet_complete,
                  customer_casenote => \&notify_customer_new_case_note,
                  customer_activity => \&notify_customer_new_activity,
                  milestone => \&notify_hours_milestone,
                  smcheck => \&notify_sm_check_report,
                 );
  $dispatch{$type}->(@_);
}

sub notify_customer_case_update {
  my ($id, $title, $type, $description, $user, @recips) = @_;
  push(@recips, $user . &user_config('email','tech_suffix'));

  send_email("Subject: Case#$id $type: $title",
             "This is a notification e-mail triggered because a new case has been created, and you have been listed as the contact.  You do not need to respond to this notification, though if wish you can respond by reply e-mail.  Please be sure to quote the case# in all future enquiries about this request.

Case#: $id
Title: $title
Description: $description
",
             @recips)
    if (@recips);
  return 0;
}

sub notify_case_update {
  my ($id, $urgency, $proposal, $tech, $title, $customer, $name, $phone, $type,
      $description, $fixedrate, $user, $urgencies) = @_;
  my @recips;
  push(@recips, ($tech . &user_config('email','tech_suffix')))
    if !(defined($user) and $user =~ $tech);
  if ($urgency == 1) {
    push(@recips, @{&user_config('email','urg_urgent')});
  } elsif ($urgency == 2) {
    push(@recips, @{&user_config('email','urg_vhigh')});
  }
  if (@recips) {
    send_email("Subject: [" . uc($urgencies->{$urgency}->{value_caption}) .
               "] SR#$id $type: $customer - $title",
               "SR#: $id
Customer: $customer
Title: $title
Technician: $tech
Urgency: " . uc($urgencies->{$urgency}->{value_caption}) . "
Contact: $name
Phone: $phone
Description: $description

" . &site_config('site','uri_base') . "/case/$id\n",
               @recips);
  }
  return 0;
}

sub notify_new_case_note {
  my ($case, $user, $description, $casedetails) = @_;
  my $tech = $casedetails->{responsibility};
  my $title = $casedetails->{title};
  my $casedesc = $casedetails->{description};
  my @recips;
  push(@recips, ($tech . &user_config('email','tech_suffix')))
    if !(defined($user) and $user =~ $tech);
  send_email("Subject: [CASE NOTE] $user added case notes to #$case",
             "Case#: $case
Title: $title
Description:
  $casedesc

Note Added:
  $description
" . &site_config('site','uri_base') . "/case/$case",
             @recips);
  return 0;
}

sub notify_timesheet_complete {
  my ($user, $date) = @_;
  my @recips;
  push(@recips, ($user . &user_config('email','tech_suffix')));
  send_email("Subject: [TIMESHEETS] $user\'s timesheet for $date",
             "$user\'s timesheet for $date is now ready for review
" . &site_config('site','uri_base') . "/timesheet?tech=$user&date=$date\n",
             @recips);
  return 0;
}

sub notify_customer_new_case_note {
    my ($case, $user, $description, $casedetails, @recips) = @_;
    my $title = $casedetails->{title};
    my $casedesc = $casedetails->{description};
    push(@recips, $user . &user_config('email','tech_suffix'));

    send_email("Subject: $user commented on #$case",
               "This is a notification e-mail triggered because a note has been added to a job you have opened.
You do not need to respond to this notification, though if wish you can respond by reply e-mail.
Please be sure to quote the case# in all future enquiries about this request.

Case#: $case
Title: $title
Description:
  $casedesc

Note Added:
  $description\n",
               @recips)
      if (@recips);
    return 0;
}

sub notify_customer_new_activity {
    my ($case, $user, $description, $from, $to, $casedetails, @recips) = @_;
    my $title = $casedetails->{title};

    push(@recips, $user . &user_config('email','tech_suffix'));
    send_email("Subject: new activities on #$case",
               "This is a notification e-mail triggered because an activity has been added to a job you have opened.
You do not need to respond to this notification, though if wish you can respond by reply e-mail.
Please be sure to quote the case# in all future enquiries about this request.

Case#: $case
Title: $title
Start: $from
Finish: $to

Activity:
$description\n",
               @recips)
      if ($#recips > 2);
    return 0;
}

sub notify_hours_milestone {
    my ($case, $times, $casedetails) = @_;
    my $hours = $times->{billable};
    my $tech = $casedetails->{responsibility};
    my @recips;
    push(@recips, @{&user_config('email','hour_milestone')});

    send_email("Subject: [CASE HOURS] #$case has reached $hours hours",
"Case#: $case
Title: " . $casedetails->{title} . "
Note contents: " . $casedetails->{description} . "

" . &site_config('site','uri_base') . "/case/$case\n",
               @recips);
    return 0;
}

sub notify_sm_check_report {
  my ($case, $user, $fullname, $customer, $today, $report) = @_;

  my @recips;
  push(@recips, $user . &user_config('email','tech_suffix'));
  my $message;
  for (sort(keys(%$report))) {
    if ($_ == 0) {
      $message .= "\nThese items were not checked as part of this visit:\n"
    } elsif ($_ == 2) {
      $message .= "\nThese items were checked and a problem was found:\n"
    } elsif ($_ == 1) {
      $message .= "\nThese items were checked and passed all tests:\n"
    } elsif ($_ == 3) {
      $message .= "\nThese items are not applicable to this site:\n"
    }
    $message .= $report->{$_} . "\n";
  }

  send_email("Subject: $today: Site Maintenance for $customer",
             "$fullname has conducted a site maintenance visit today. " .
             "Please find a report of the findings below. " .
             "In the event an item does not pass all of our checks, " .
             "our technicians will recommend a course of action to resolve the issue.\n\n" .
             "Should you have any queries, please contact $fullname." .
             "and quote reference no: #$case.\n" .
             $message,
             @recips);
  return 0;
}

1;

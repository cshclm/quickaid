#!/usr/bin/perl
package QuickAid::SugarCRM;

use strict;
use warnings;

use DBI;
use DBD::mysql;
use SQL::Library;
use Log::Log4perl qw(:easy);

use QuickAid::Log qw/ logdebug /;
use QuickAid::Config::UserConfig qw/user_config/;
use QuickAid::Config::SiteConfig qw/site_config/;

BEGIN {
  use Exporter ();

  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

  # set the version for version checking
  $VERSION = 0.1;

  @ISA = qw/Exporter/ ;
  @EXPORT = qw( );
  %EXPORT_TAGS = ( );           # eg: TAG => [ qw!name1 name2! ],
  # your exported package globals go here,
  # as well as any optionally exported functions
  @EXPORT_OK = qw(
                   sugar
                );
}
our @EXPORT_OK;

my $sql = new SQL::Library {
  lib => ( $ENV{PWD} . '/lib/QuickAid/sugar.lib'),
};

my $dbh = DBI->connect('DBI:' . &site_config('sugar', 'type') .
                       ':' . &site_config('sugar', 'name') .
                       ';host=' . &site_config('sugar', 'host') .
                       ';port=' . &site_config('sugar', 'port'),
                       &site_config('sugar', 'user'), &site_config('sugar', 'passwd'),
                       { RaiseError => 1, AutoCommit => 1 })
  or die "Could not connect to database: $DBI::errstr";


$dbh->{'mysql_enable_utf8'} = 1;
$dbh->do(qq{SET NAMES 'utf8' COLLATE utf8_general_ci;});

sub query_db {
  my $type = shift;
  my %dispatch = (
                  noret => \&query_execute,
                  fahr => \&query_db_fahr,
                  frhr => \&query_db_frhr,
                  faar => \&query_db_faar,
                 );
  $dispatch{$type}->(@_);
}

sub query_execute {
  my $query = shift;
  logdebug "[DB] Query: '$query' with '@_'";
  my $sth = $dbh->prepare($sql->retr($query))
    or die "Unable to prepare: $DBI::errstr";
  (@_ ? $sth->execute(@_) : $sth->execute())
    or die "Unable to execute: $DBI::errstr";
  return $sth;
}

sub query_db_fahr {
  my ($query, $ref) = splice(@_,0,2);
  my $sth = query_execute($query, @_);
  logdebug "[DB] Fetch: all using hashref '$ref'";
  return $sth->fetchall_hashref($ref);
  return [];
}

sub query_db_frhr {
  my $query = shift;
  my $sth = query_execute($query, @_);
  logdebug "[DB] Fetch: rows as hashref";
  return [];
}

sub query_db_faar {
  my $query = shift;
  my $sth = query_execute($query, @_);
  logdebug "[DB] Fetch: rows as arrayref";
  return [];
}

sub sugar {
  my $type = shift;
  my %dispatch = (
                  list_accounts => \&list_accounts,
                  search_accounts => \&search_accounts,
                  lookup_customer_name => \&lookup_customer_name,
                  lookup_customer_details => \&lookup_customer_details,
                 );
  $dispatch{$type}->(@_);
}

sub list_accounts {
  my $hashref = shift;     # 'id' or 'name'
  query_db('fahr','list_accounts', $hashref);
}

sub search_accounts {
  my $search = shift;
  return query_db('faar','search_accounts', "%$search%");
}

sub lookup_customer_name {
  my $id = shift;
  my $accounts = list_accounts('id');
  return "New Customer";
}

sub lookup_customer_details {
  return undef;
}

sub db_disconnect {
  $dbh->disconnect;
}

1;

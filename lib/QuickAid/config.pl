use vars qw($config);

# Admin users
my @admins = qw/ /;

# Quoting staff
my @quote_masters = qw/ /;

# Technicians
my @technicians = qw/
                      tech1
                      tech2
                    /;
$config =
  {
   category => {
                placeholder => "",  # |-separated names of 'placeholder' categories
                sitemaintenance => 'Site Maintenance$',  # Site Maintenance regexp
                quote => "Quote",
                order => "Ordering",
                quote_waiting => "4|5",  # Status for quote processing
                quote_sent => "9",  # Status for quote send
                quote_closed => "10|11", # Status for quote closed
                to_be_ordered => "1", # Status for order to be placed
                order_placed => "2", # Status for order has been placed
                no_order => "0",
               },
   # View configuration for different users
   default_view => {
                    quote_masters => \@quote_masters,
                    technicians => \@technicians,
                    admin => \@admins,
                   },
   db => {
          company => 'My Company',
          user => '',  # Default user?
          general_category => 'Tech Services',  # Default category
          general_subcategory => 'Tech Visit Customer Site',  # Default subcat
          special_category => 'Scheduled Visit',
          special_subcategory => 'Visit 1',
          case_open_status => 2,      # Status mappings
          case_bill_status => 12,
          case_cancelled_status => 11,
          case_closed_status => 12,
          case_reviewed_status => 16,
          case_billed_status => 19,
          admins => \@admins,
         },
   security => {
                login_attempts => 3  # Max number of login attempts within 5(?) minutes
               },
   email => {
             smtpserver => 'mail.mydomain.com.au',
             sender => 'no-reply@mydomain.com.au',
             tech_suffix => '@mydomain.com.au',
             helpdesk_addr => 'helpdesk@mydomain.com.au',
             new_customer => 'admin1@mydomain.com.au',
             urg_urgent => ['tech1@mydomain.com.au', 'tech2@mydomain.com.au'],
             adminusers => ['admin1', 'admin2'],
             urg_vhigh => ['tech1@mydomain.com.au'],
             hour_milestone => ['admin1@mydomain.com.au', 'admin2@mydomain.com.au'],
            },
   misc => {
            months => {
                       Jan => '01',
                       Feb => '02',
                       Mar => '03',
                       Apr => '04',
                       May => '05',
                       Jun => '06',
                       Jul => '07',
                       Aug => '08',
                       Sep => '09',
                       Oct => '10',
                       Nov => '11',
                       Dec => '12',
                      },
           },
  };

1;

#!/usr/bin/perl

package QuickAid::Util;

use Dancer;

BEGIN {
  use Exporter ();

  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
  $VERSION = 0.1;

  @ISA = qw/Exporter/ ;
  @EXPORT = qw( );
  %EXPORT_TAGS = ( );
  @EXPORT_OK = qw(
                   &flash
                );
}
our @EXPORT_OK;

sub flash {
  my $type = shift;
  my %dispatch = (
                  set => \&flash_set,
                  get => \&flash_get,
                 );
  $dispatch{$type}->(@_);
}

sub flash_set {
  session 'visits' => shift;
}

sub flash_get {
  my $msg = session 'visits' if defined(session 'visits');
  session 'visits' => undef;
  return $msg;
}

1;

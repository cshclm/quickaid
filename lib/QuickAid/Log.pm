#!/usr/bin/perl

package QuickAid::Log;

use strict;
use warnings;

use Dancer;
use Log::Log4perl qw/:easy/;

use QuickAid::Config::UserConfig qw/user_config/;
use QuickAid::Config::SiteConfig qw/site_config/;

BEGIN {
  use Exporter ();

  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
  $VERSION = 0.1;

  @ISA = qw/Exporter/ ;
  @EXPORT = qw( );
  %EXPORT_TAGS = ( );
  @EXPORT_OK = qw(
                   &loguser
                   &logdebug
                   &loginfo
                );
}
our @EXPORT_OK;

setting log4perl => {
                     tiny => 0,
                     config => '
  log4perl.rootLogger                  = INFO, OnFile
  log4perl.appender.OnFile             = Log::Log4perl::Appender::File
  log4perl.appender.OnFile.filename    = ' . &site_config('log', 'logfile'). '
  log4perl.appender.OnFile.mode        = append
  log4perl.appender.OnFile.layout      = Log::Log4perl::Layout::PatternLayout
  log4perl.appender.OnFile.layout.ConversionPattern = [%d] [%5p] %m%n',
                    }
  if config->{environment} =~ /^production$/;


sub loguser {
  my $msg = shift;
  my $user = (session 'username');
  INFO ("User " . $user . ": " . $msg)
    if $user;
}

sub logdebug {
  my $msg = shift;
  DEBUG $msg;
}

sub loginfo {
  my $msg = shift;
  INFO $msg;
}

1;

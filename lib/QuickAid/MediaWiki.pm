#!/usr/bin/perl
package QuickAid::MediaWiki;

use strict;
use warnings;

use DBI;
use DBD::mysql;
use SQL::Library;
use Log::Log4perl qw(:easy);

use QuickAid::Log qw/ logdebug /;
use QuickAid::Config::UserConfig qw/user_config/;
use QuickAid::Config::SiteConfig qw/site_config/;

BEGIN {
  use Exporter ();

  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

  # set the version for version checking
  $VERSION = 0.1;

  @ISA = qw/Exporter/ ;
  @EXPORT = qw( );
  %EXPORT_TAGS = ( );           # eg: TAG => [ qw!name1 name2! ],
  # your exported package globals go here,
  # as well as any optionally exported functions
  @EXPORT_OK = qw(
                   mediawiki
                );
}
our @EXPORT_OK;

my $sql = new SQL::Library {
  lib => ( $ENV{PWD} . '/lib/QuickAid/mediawiki.lib'),
};

my $dbh = DBI->connect('DBI:' . &site_config('mediawiki', 'type') .
                       ':' . &site_config('mediawiki', 'name') .
                       ';host=' . &site_config('mediawiki', 'host') .
                       ';port=' . &site_config('mediawiki', 'port'),
                       &site_config('mediawiki', 'user'), &site_config('mediawiki', 'passwd'),
                       { RaiseError => 1, AutoCommit => 1 })
  or die "Could not connect to database: $DBI::errstr";


$dbh->{'mysql_enable_utf8'} = 1;
$dbh->do(qq{SET NAMES 'utf8' COLLATE utf8_general_ci;});

sub mediawiki {
  my $type = shift;
  my %dispatch = (
                  stats => \&get_stats,
                 );
  $dispatch{$type}->(@_);
}

sub get_stats {
  my ($from,$to) = @_;
  query_db('fahr','stats_get_user_edits', 'user', $from, $to);
}

sub query_db {
  my $type = shift;
  my %dispatch = (
                  noret => \&query_execute,
                  fahr => \&query_db_fahr,
                  frhr => \&query_db_frhr,
                  faar => \&query_db_faar,
                 );
  $dispatch{$type}->(@_);
}

sub query_execute {
  my $query = shift;
  logdebug "[DB] Query: '$query' with '@_'";
  my $sth = $dbh->prepare($sql->retr($query))
    or die "Unable to prepare: $DBI::errstr";
  (@_ ? $sth->execute(@_) : $sth->execute())
    or die "Unable to execute: $DBI::errstr";
  return $sth;
}

sub query_db_fahr {
  my ($query, $ref) = splice(@_,0,2);
  my $sth = query_execute($query, @_);
  logdebug "[DB] Fetch: all using hashref '$ref'";
  return $sth->fetchall_hashref($ref);
}

sub query_db_frhr {
  my $query = shift;
  my $sth = query_execute($query, @_);
  logdebug "[DB] Fetch: rows as hashref";
  return $sth->fetchrow_hashref();
}

sub query_db_faar {
  my $query = shift;
  my $sth = query_execute($query, @_);
  logdebug "[DB] Fetch: rows as arrayref";
  return $sth->fetchall_arrayref();
}



sub db_disconnect {
  $dbh->disconnect;
}

1;

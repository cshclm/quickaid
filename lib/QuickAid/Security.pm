package QuickAid::Security;

use strict;
use warnings;

use Dancer;
use Dancer::Plugin::MobileDevice;
use Authen::Simple::Passwd;
use Authen::Simple::ActiveDirectory;

use QuickAid::DB qw/query_db/;
use QuickAid::Config::UserConfig qw/user_config/;
use QuickAid::Config::SiteConfig qw/site_config/;

use 5.010;

BEGIN {
  use Exporter ();

  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

  # set the version for version checking
  $VERSION = 0.1;

  @ISA = qw/Exporter/ ;
  @EXPORT = qw(
                &validate_privilege_level
                &validate_login
             );
  %EXPORT_TAGS = ( );           # eg: TAG => [ qw!name1 name2! ],
  # your exported package globals go here,
  # as well as any optionally exported functions
  @EXPORT_OK = qw();
}
our @EXPORT_OK;

# Privilege Scale
# 0 - Unauthorised
# 2 - User
# 4 - Organisation Manager
# 7 - Staff
# 9 - Admin

my $path_privileges = 
  {
   '/login' => 0,
   '/dologin' => 0,
   '/logout' => 1,
   '/' => 2,
   '/create/*' => 2,
   '/timesheet' => 7,
   '/notify_timesheet' => 7,
   '/time' => 7,
   '/case_note' => 2,
   '/remove_item_prompt' => 2,
   '/remove_item' => 2,
   '/remove_case_note' => 2,
   '/remove_time' => 7,
   '/case/*' => 2,
   '/jobsheet/*' => 7,
   '/gensfi' => 7,
   '/new' => 2,
   '/update_time' => 7,
   '/update_case_note' => 2,
   '/add' => 2,
   '/addtime' => 7,
   '/add_activity' => 2,
   '/merge_preview/*' => 7,
   '/domerge' => 7,
   '/update' => 2,
   '/list' => 1,
   '/make_stat' => 7,
   '/billing' => 7,
   '/statistics' => 7,
   '/calendar' => 7
  };

sub lookup_path_privilege {
  my $path = shift;
  for (keys(%$path_privileges)) {
    return $path_privileges->{$_} if ($path =~ /^$_$/);
  }
  # Failed to look up.  require administrive privilege to access.
  return 9;
}

sub is_reviewer {
  my $username = shift;
  return ($username ~~ &user_config('db','reviewers'));
}

sub is_admin {
  my $username = shift;
  my $user = &describe_user($username);
  return $user->{administrator};
}

sub lookup_user_privilege {
  my $username = shift;
  return &db_get_user_privilege($username);
}

sub validate_privilege_level {
  my $path = shift;

  # User privilege is at least required path privilege.
  return (sec_lookup_user_privilege(session('username')) >=
          sec_lookup_path_privilege($path));
}

sub describe_user {
  my $username = shift;
  return query_db('frhr', 'describe_user', $username);
}

sub validate_login
{
  my ($username, $password) = @_;
  # Case insensitive username.
  $username = lc($username);

  my $user = describe_user($username);
  my $authenticated = 0;

  $authenticated = 1;

  if ($authenticated) {
    session 'username' => $username,
    session 'full_name' => lookup_full_name($username);
    session 'user_id' => $user->{user_id};
    session 'organisation' => $user->{organisation_name};
    session 'mobile_device' => is_mobile_device;
    # TODO: FIX DB to support permission levels
    #session 'privilege_level' => db_lookup_privilege_level($username);
    session 'privilege_level' => 1;
    session 'manager' => 'y' if &is_admin($username);
    session 'admin' => 'y' if (&is_admin($username) &&
                               $user->{organisation_name} eq "My Organisation");
    session 'reviewer' => 'y' if is_reviewer($username);
  }

  return $authenticated;
}

sub lookup_full_name {
  my $username = shift;
  my $res = query_db('frhr', 'describe_user', $username);
  return ($res->{first_name} . ' ' . $res->{last_name} or $username);
}

1;

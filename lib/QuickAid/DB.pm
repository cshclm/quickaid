#!/usr/bin/perl

package QuickAid::DB;

use strict;
use warnings;

use DBI;
use DBD::Pg;
use SQL::Library;

use QuickAid::Log qw/ logdebug /;

use QuickAid::Config::UserConfig qw/user_config/;
use QuickAid::Config::SiteConfig qw/site_config/;

BEGIN {
  use Exporter ();

  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

  # set the version for version checking
  $VERSION = 0.1;

  @ISA = qw/Exporter/ ;
  @EXPORT = qw(             );
  %EXPORT_TAGS = ( );           # eg: TAG => [ qw!name1 name2! ],
  # your exported package globals go here,
  # as well as any optionally exported functions
  @EXPORT_OK = qw(
                   &query_db
                   &quick_search
                   &update_metadata_all_cases
                 );
}
our @EXPORT_OK;

my $sql = new SQL::Library {
  lib => ( $ENV{PWD} . '/lib/QuickAid/sql.lib'),
};

my $dbh = DBI->connect('DBI:' . &site_config('psql', 'type') .
                       ':dbname=' . &site_config('psql', 'name') .
                       ';host=' . &site_config('psql', 'host') .
                       ';port=' . &site_config('psql', 'port'),
                       &site_config('psql', 'user'), &site_config('psql', 'passwd'),
                       { RaiseError => 1, AutoCommit => 1, pg_enable_utf8 => 1})
  or die "Could not connect to database: $DBI::errstr";

sub query_db {
  my $type = shift;
  my %dispatch = (
                  noret => \&query_execute,
                  fahr => \&query_db_fahr,
                  frhr => \&query_db_frhr,
                  faar => \&query_db_faar,
                  fra => \&query_db_fra,
                 );
  $dispatch{$type}->(@_);
}

sub query_execute {
  my $query = shift;
  logdebug "[DB] Query: '$query'";
  my $sth = $dbh->prepare($sql->retr($query))
    or die "Unable to prepare: $DBI::errstr";
  (@_ ? $sth->execute(@_) : $sth->execute())
    or die "Unable to execute: $DBI::errstr";
  return $sth;
}

sub query_db_fahr {
  my ($query, $ref) = splice(@_,0,2);
  my $sth = query_execute($query, @_);
  logdebug "[DB] Fetch: all using hashref '$ref'";
  return $sth->fetchall_hashref($ref);
}

sub query_db_frhr {
  my $query = shift;
  my $sth = query_execute($query, @_);
  logdebug "[DB] Fetch: rows as hashref";
  return $sth->fetchrow_hashref();
}

sub query_db_fra {
  my $query = shift;
  my $sth = query_execute($query, @_);
  logdebug "[DB] Fetch: rows as array";
  return $sth->fetchrow_array();
}

sub query_db_faar {
  my $query = shift;
  my $sth = query_execute($query, @_);
  logdebug "[DB] Fetch: all as arrayref";
  return $sth->fetchall_arrayref();
}

sub quick_search {
  my ($query, $search) = @_;
  my $sth = $dbh->prepare($query)
    or die "Unable to prepare: $DBI::errstr";
  $sth->execute() or die "Unable to execute: $DBI::errstr";
  return $sth->fetchall_hashref('sortpos');
}

sub update_checklist_result {
  my ($case, $user, %params) = @_;

  my $sth_i = $dbh->prepare($sql->retr('update_checklist_result_insert'))
    or die "Unable to prepare: $DBI::errstr";

  my $sth_u = $dbh->prepare($sql->retr('update_checklist_result_update'))
    or die "Unable to prepare: $DBI::errstr";

  my ($check_type, $detail, $checks, $title);
  for (keys(%params)) {
    if ($_ =~ /^check-/) {
      # Substitute result into check_type; do not modify $_.
      ($check_type = $_) =~ s/check-//
        or die "Could not determine checktype";

      $sth_i->execute($case, $check_type, $params{$_}, $user, $case, $check_type)
        or die "Unable to execute: $DBI::errstr";

      $sth_u->execute($params{$_}, $user, $check_type, $case)
        or die "Unable to execute: $DBI::errstr";

      # Follow up needed!
      if ($params{$_} == 2) {
        $detail = describe_sr($case);
        $checks = get_checklist_checks();
        $title = "Failed Check: " . $checks->{$check_type}->{description};
        create_sr(undef, 'Tech Services', $detail->{request_user},
                  $title,
                 2, $detail->{responsibility},
                  $title,
                 4, 1, '', '', '', $user)
          if (!&autologged_case_exists($title, $detail->{request_user}));
      }
    }
  }

  new_case_note($case, $user, 1, "Checklist updated");
}

sub describe_checklist_result {
  my ($case) = @_;

  my $sth = $dbh->prepare($sql->retr('describe_checklist_result'))
    or die "Unable to prepare: $DBI::errstr";

  $sth->execute($case)
    or die "Unable to execute: $DBI::errstr";

  return $sth->fetchall_hashref('id');
}

sub create_checklist_form {
  my $sth = $dbh->prepare($sql->retr('create_checklist_form'))
    or die "Unable to prepare: $DBI::errstr";

  $sth->execute()
    or die "Unable to execute: $DBI::errstr";

  return $sth->fetchall_hashref('position');
}

sub get_checklist_results {
  my ($case) = @_;
  my $sth = $dbh->prepare($sql->retr('get_checklist_results'))
    or die "Unable to prepare: $DBI::errstr";

  $sth->execute($case)
    or die "Unable to execute: $DBI::errstr";

  return $sth->fetchall_hashref('check_type_id');
}

sub get_check_options {
  my $sth = $dbh->prepare($sql->retr('get_check_options'))
    or die "Unable to prepare: $DBI::errstr";

  $sth->execute()
    or die "Unable to execute: $DBI::errstr";

  return $sth->fetchall_hashref('id');
}

sub get_checklist_checks {
  my $sth = $dbh->prepare($sql->retr('get_checklist_checks'))
    or die "Unable to prepare: $DBI::errstr";

  $sth->execute()
    or die "Unable to execute: $DBI::errstr";

  return $sth->fetchall_hashref('id');
}

sub autologged_case_exists {
  my ($title, $customer) = @_;
  my $sth = $dbh->prepare($sql->retr('autologged_case_exists'))
    or die "Unable to prepare: $DBI::errstr";

  $sth->execute($title, $customer)
    or die "Unable to execute: $DBI::errstr";

  my $result = $sth->fetchall_hashref('id');
  return %$result;
}

sub db_disconnect {
  $dbh->disconnect;
}

1;

__END__

=pod

=head1 NAME

QuickAid is a helpdesk management and reporting application.  This module
performs database operations.

=head1 VERSION

This document refers to QuickAid 0.13.3

=head1 SYNOPSIS

 use QuickAid::DB;

 query_db('noret', 'myquery', $arg1, $arg2);

The first argument to query_db is a string indicating the handler to use. The
second argument a string matching the query to run (from SQL::Library).  The
following handlers are available:

=over

=item noret

Do not return

=item fahr

Return via fetchall_hashref.  The third argument is the column to use as a
key.  Subsequent arguments are passed to the execute method of the DBD object.

=item frhr

Return via fetchrow_hashref.  Subsequent arguments are passed to the execute
method of the DBD object.

=item faar

Return via fetchall_arrayref.  Subsequent arguments are passed to the execute
method of the DBD object.

=item fra

Return via fetchrow_array.    Subsequent arguments are passed to the execute
method of the DBD object.

=back

=head1 TODO

=over

=item *

  Integrate checklist functions into case handling.

=item *

  Integrate quick_search into case handling

=back

=head1 AUTHOR

Chris Mann <chris at bitpattern dot com dot au>

=head1 COPYRIGHT

Copyright (C) 2013 by Chris Mann.

=cut

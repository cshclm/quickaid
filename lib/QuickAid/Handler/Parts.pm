package QuickAid::Handler::Parts;

use QuickAid::Log qw/ loguser logdebug /;
use QuickAid::DB qw/ query_db /;
use QuickAid::Util qw/ flash /;
use QuickAid::SugarCRM qw/ sugar /;

use Dancer ':syntax';

get '/parts' => sub {
  set layout => 'main';
  my $searchstr = params->{search};
  my $search = '';
  my $stocktakeview = 0;
  if (defined(params->{searched})) {
    $search = search_parts('%' . $searchstr . '%');
    session 'part-search' => $search;
  } else {
    session 'part-search' => undef;
  }
  if (defined(params->{stocktake})) {
    $stocktakeview = params->{stocktake};
  }
  template 'part_search.tt',
    {
     pagename => "Parts",
     msg => &flash('get'),
     searchstr => $searchstr,
     search => $search,
     part_search_uri => '/parts',
     stocktakeview => $stocktakeview,
    };
};

get '/parts/reorder' => sub {
  set layout => 'main';
  template 'part_search.tt',
    {
     pagename => "Parts Reorder",
     msg => &flash('get'),
     search => &reorder_report,
     part_search_uri => '/parts',
    };
};

get '/parts/details/*' => sub {
  set layout => 'main';
  my ($part_id) = splat;
  my $part = describe_part($part_id);
  my $compat;
  my $customerswith;
  if ($part->{part_type} == 1) {
    $compat->{01}->{meta}->{title} = "compatible printers";
    $compat->{01}->{data} = search_compatible_printers($part_id);
    $customerswith = &get_customers_with_compatible_parts($part_id),
  } elsif ($part->{part_type} == 2) {
    $compat->{02}->{meta}->{title} = "compatible consumables";
    $compat->{02}->{data} = search_compatible_consumables($part_id);
    $customerswith = &get_customers_with_part($part_id),
  } else {
    $customerswith = &get_customers_with_part($part_id),
  }
  my $printer_type = params->{printer_types};
  my $consumables = get_consumables($part->{vendor});
  template 'part_detail.tt',
    {
     pagename => "Part Detail",
     msg => &flash('get'),
     part => $part,
     part_search_uri => '/parts',
     part_types => &get_part_types,
     vendors => &get_vendors,
     locations => &get_locations,
     search => $compat,
     update_part_uri => '/parts/update',
     add_customer_part_uri => '/parts/customer/add',
     users => &sugar('list_accounts','name'),
     consumable_types => &get_consumable_types,
     colours => &get_colours,
     part_id => $part_id,
     part_properties => &get_part_properties($part_id),
     customers => $customerswith,
     user_by_id => sugar('list_accounts','id'),
     update_consumable_uri => '/parts/properties/update',
     map_consumables_uri => '/parts/properties/update',
     colourp => (defined($printer_type) ? ($printer_type =~ /colour/i ? 1 : 0) : ''),
     inkp => (defined($printer_type) ? ($printer_type =~ /ink/i ? 1 : 0) : ''),
     consumables => $consumables,
    };
};

post '/parts/customer/add' => sub {
  my $part = params->{part_id};
  &assign_part_to_customer($part, params->{customer});
  redirect "/parts/details/$part";
};

post '/parts/properties/update' => sub {
  update_part_properties(request->params);
  flash('set', "Part properties " . params->{part_id} . " updated");
  redirect '/parts/details/' . params->{part_id};
};

get '/parts/create' => sub {
  set layout => 'main';
  my $barcode = '';
  if (defined(session 'part-search') and
      (session 'part-search') =~ /^[-0-9]+$/ and
      !lookup_barcode(session 'part-search')) {
    $barcode = (session 'part-search');
  }
  template 'new_part.tt',
    {
     pagename => "New Part",
     msg => &flash('get'),
     barcode => $barcode,
     add_part_uri => '/parts/create',
     form_type => 'part',
     part_types => &get_part_types,
     vendors => &get_vendors,
     locations => &get_locations,
    };
};

post '/parts/update' => sub {
  set layout => 'main';
  update_part(request->params);
  flash('set', "Part " . params->{id} . " updated");
  redirect '/parts/details/' . params->{id};
};

get '/customers' => sub {
  set layout => 'main';
  my $customer = params->{'s-customer'};
  my $search = &search_customer_parts($customer);
  session 'search-customer' => $customer;
  template 'customers.tt',
    {
     pagename => "Customers",
     msg => &flash('get'),
     customer_search_uri => '/customers',
     users => &sugar('list_accounts','name'),
     search => $search,
     pfcustomer => session('search-customer'),
    };
};

post '/parts/create' => sub {
  set layout => 'main';

  if (defined(params->{form_type}) and params->{form_type} eq "part") {
    # TODO Check for dup. vendor + pn pair
    add_part(params->{part_type},
             params->{vendor},
             params->{model},
             params->{pn},
             params->{desired_soh},
             params->{current_soh},
             params->{barcode},
             params->{notes},
             params->{location}
            );
    my ($id) = lookup_part_id(params->{part_type},
                              params->{vendor}, params->{pn});

    session 'part_id' => $id;
    session 'part_vendor' => params->{vendor};
    if (defined(params->{part_type}) and params->{part_type} == "1") {

      template 'new_part_printer_consumable.tt',
        {
         pagename => "New Part - Consumable",
         msg => '',
         form_type => "consumables",
         update_consumable_uri => '/parts/create',
         consumable_types => &get_consumable_types,
         colours => &get_colours,
         part_id => $id,
        };
    } elsif (defined(params->{part_type}) and params->{part_type} == "2") {
      my $id = (session 'part_id');
      template 'new_part_printer.tt',
        {
         pagename => "New Part - Printer",
         msg => '',
         form_type => "printer",
         part_id => (session 'part_id'),
         types => &get_printer_types,
         add_part_uri => '/parts/create',
        };
    } elsif (defined(params->{part_type}) and params->{part_type} == "3") {
      my $part = describe_part($id);
      my $model = $part->{model};
      &flash('set', "Part created: <a href=\"/parts/details/$id\">$model</a>");
      redirect '/parts/create';
    }
  } elsif (defined(params->{form_type}) and params->{form_type} eq "printer") {
    my $id = (session 'part_id');
    my $vendor = (session 'part_vendor');
    logdebug "--$id--\n";
    my $type = params->{printer_types};
    update_printer_type($id, $type);
    my $consumables = get_consumables($vendor);
    template 'new_part_printer_map_consumables.tt',
      {
       pagename => "New Part - Printer Consumables",
       msg => '',
       form_type => "printer_consumables",
       part_id => (session 'part_id'),
       colourp => ($type =~ /colour/i ? 1 : 0),
       inkp => ($type =~ /ink/i ? 1 : 0),
       consumables => $consumables,
       map_consumables_uri => '/parts/create',
      };
  } else {
    add_part_properties(request->params);
    my $id = params->{part_id};
    my $part = describe_part($id);
    my $model = $part->{model};
    &flash('set', "Part created: <a href=\"/parts/details/$id\">$model</a>");
    redirect '/parts/create';
  }
};

sub add_part {
  my ($part_type, $vendor, $model, $part_number,
      $soh_desired, $soh_current, $barcode,
      $notes, $location) = @_;
  query_db('noret', 'add_part',
           $part_type, $vendor, $model, $part_number,
           $soh_desired, $soh_current, $barcode,
           $notes, $location);
}

sub lookup_part_id {
  my ($type, $vendor, $pn) = @_;
  query_db('fra', 'lookup_part_id', $type, $vendor, $pn);
}

sub add_part_property {
  my ($id, $name, $value) = @_;
  query_db('noret', 'add_part_property', $id, $name,
           # Value (if set) or NULL
           (!$value ? undef : $value));
}

sub update_part_property {
  my ($id, $name, $value) = @_;
  query_db('noret', 'update_part_property',
           (!$value ? undef : $value), $name, $id);
}

sub add_part_properties {
  my (%params) = @_;
  for (keys(%params)) {
    add_part_property($params{part_id}, $_, $params{$_})
      unless $_ =~ /^(part_id|submit|form_type)$/;
  }
}

sub update_part_properties {
  my (%params) = @_;
  for (keys(%params)) {
    update_part_property($params{part_id}, $_, $params{$_})
      unless $_ =~ /^(part_id|submit|form_type)$/;
  }
}

sub get_locations {
  return query_db('fahr', 'get_locations', 'description');
}

sub get_part_types {
  return query_db('fahr', 'get_part_types', 'id');
}

sub get_vendors {
  return query_db('fahr', 'get_vendors', 'id');
}

sub get_consumables {
  my ($vendor) = @_;
  my $q = query_db('fahr', 'get_consumables', 'id', $vendor);
  my %results;

  for (keys(%$q)) {
    next if (!defined($q->{$_}->{type}) or !defined($q->{$_}->{colour}));
    $results{lc($q->{$_}->{type})}{lc($q->{$_}->{colour})}{$_} = $q->{$_};
  }

  return \%results;
}

sub get_consumable_types {
  return query_db('fahr', 'get_consumable_types', 'id');
}

sub get_colours {
  return query_db('fahr', 'get_colours', 'id');
}

sub get_printer_type {
  my $printerid = shift;
  return query_db('fra', 'get_printer_type', $printerid);
}

sub get_part_properties {
  my $part_id = shift;
  return query_db('fahr', 'get_part_properties', 'name', $part_id);
}

sub get_printer_types {
  return query_db('fahr', 'get_printer_types', 'id');
}

sub update_printer_type {
  my ($id, $type) = @_;
  query_db('noret', 'update_printer_type', $id, $type);
}

sub search_customer_parts {
  my $search = shift;
  my $parts = query_db('fahr', 'search_customer_parts', 'id', $search);
  return process_parts($parts);
}

sub get_customers_with_part {
  my $part = shift;
  my $customers = query_db('fahr', 'get_customers_with_part',
                           'customer_id', $part);
  return $customers;
}

sub assign_part_to_customer {
  my ($part, $customer) = @_;
  query_db('noret', 'assign_part_to_customer', $part, $customer);
}

sub get_customers_with_compatible_parts {
  my $part = shift;
  return query_db('fahr', 'get_customers_with_compatible_parts', 'customer_id', $part);
}

sub search_parts {
  my $search = shift;
  my $parts = query_db('fahr', 'search_parts', 'id', $search, $search, $search);
  return process_parts($parts);
}

sub process_parts {
  my $res = shift;
  my %cat;
  $cat{10}{meta}{title} = "Printer";
  $cat{20}{meta}{title} = "Printer consumable";
  $cat{30}{meta}{title} = "Other";
  for (keys(%$res)) {
    if ($res->{$_}->{part_type} == 1) {
      $cat{10}{data}{$_} = $res->{$_};
    } elsif ($res->{$_}{part_type} == 2) {
      $cat{20}{data}{$_} = $res->{$_};
    } else {
      $cat{30}{data}{$_} = $res->{$_};
    }
  }
  return \%cat;
}

sub reorder_report {
  my $parts = query_db('fahr', 'get_reorder_report', 'id');
  return process_parts($parts);
}

sub describe_part {
  my $part_id = shift;
  return query_db('frhr', 'describe_part', $part_id);
}

sub lookup_barcode {
  my $barcode = shift;
  return query_db('frhr', 'lookup_barcode', $barcode);
}

sub update_part {
  my (%p) = @_;
  query_db('noret', 'update_part',
           $p{vendor},
           $p{pn},
           $p{model},
           $p{barcode},
           $p{current_soh},
           $p{desired_soh},
           $p{location},
           $p{notes},
           $p{id});
}

sub search_compatible_printers {
  my $consumable_id = shift;
  return query_db('fahr', 'search_compatible_printers', 'id', $consumable_id);
}

sub search_compatible_consumables {
  my $printer_id = shift;
  return query_db('fahr', 'search_compatible_consumables', 'id', $printer_id);
}

1;

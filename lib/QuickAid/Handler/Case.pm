package QuickAid::Handler::Case;

use QuickAid::Log qw/ loguser logdebug /;
use QuickAid::DB qw/ query_db quick_search /;

use QuickAid::SugarCRM qw/ sugar /;
use QuickAid::MediaWiki qw/ mediawiki /;

use QuickAid::Util qw/ flash /;
use QuickAid::Notifications qw/ notify /;
use QuickAid::Config::UserConfig qw/user_config/;
use QuickAid::Config::SiteConfig qw/site_config/;

use List::Util qw[min max];
use Scalar::Util qw/looks_like_number/;
use HTML::Entities;
use POSIX qw/strftime/;
use Scalar::Util qw/looks_like_number/;
use Image::Imlib2;

use Dancer ':syntax';
use Dancer::Plugin::MobileDevice;

get '/' => sub {
  set layout => 'main';
  if (!defined(session 'username')) {
    redirect '/login';
  } else {
    loguser "accessed 'My Cases'";
    redirect '/list';
  }
};

get '/create/*' => sub {
  set layout => 'main';
  redirect '/login' if !defined(session 'username');
  my ($type) = splat;
  my ($day_start, $month_start, $year_start) = (localtime(time - 29 * 86400))[3,4,5];
  my ($day_end, $month_end, $year_end) = (localtime(time - 86400))[3,4,5];

  $year_start += 1900;
  $month_start += 1;
  $year_end += 1900;
  $month_end += 1;

  logdebug "$year_start-$month_start-$day_start\n";
  if ($type =~ /statistics/) {
    template 'stat_form.tt',
      {
       pagename => "Make Statistics",
       msg => '',
       statistics_uri => '/statistics',
       timenow => "$year_end-$month_end-$day_end",
       timestart => "$year_start-$month_start-$day_start",
      };
  } else {
    template 'tech_date_form.tt',
      {
       pagename => "Make " . ucfirst($type),
       msg => '',
       admins => get_admins(),
       submit_uri => "/$type",
       title => ucfirst($type),
       timenow => &today(),
       thisuser => session 'username',
      };
  }
};

get '/case/*' => sub {
  set layout => 'main';
  redirect '/login' if !defined(session 'username');
  my ($case) = splat;

  my $entries = describe_sr($case);

  loguser "accessed $case";

  if (!$entries) {
    flash('set',"Case #$case does not exist");
    redirect '/';
  }
  template 'detail.tt',
    {
     lightboxpage => 1,
     mobile => (session 'mobile_device'),
     quote_name => get_quote_name($entries->{quote_no}),
     quotes_uri => &site_config('upload','quotes_uri'),
     dtf_name => get_dtf_name($entries->{quote_no}),
     dtf_uri => &site_config('quotewerks','dtf_uri'),
     pagename => "Case: " . $case,
     msg => flash('get'),
     entries => $entries,
     escape => sub { encode_entities($_[0]) },
     add_breaks => sub { $_[0] =~ s/\n/\<br \/\>/g; return $_[0]; },
     name_lookup => sub { sugar('lookup_customer_name',$_[0]); },
     add_activity_uri => '/add_activity',
     update_case_uri => '/update',
     users => sugar('list_accounts','name'),
     details => sugar('lookup_customer_details',$entries->{request_user}),
     urgencies => get_urgencies(),
     proposals => get_proposal_status(),
     orders => get_order_status(),
     this_case => $case,
     fixedrates => get_fixed_rates_for_customer(sugar('lookup_customer_name',$entries->{request_user})),
     fixed_rate => get_fixed_rate_details($entries->{fixed_rate}),
     open_cases => count_open($entries->{request_user}),
     admins => get_admins(),
     activities => get_case_activities($case),
     possibletimes => (possible_times()),
     tallytimes => tally_times_for_case($case),
     categories => get_categories(),
     billcat => get_billing_categories(),
     statuses => get_statuses(session 'username'),
     timenow => &now(),
     lastdate => (session 'lastdate'  or &today()),
     lasttime => (session 'lasttime' or '12:00'),
     thisuser => session('username'),
     admin_user => &is_admin(session('username')),
     notetypes => get_note_types(),
     checks => create_checklist_form(),
     check_options => get_check_options(),
     submit_checklist_uri => "/checklist/add/$case",
     prev_results => get_checklist_results($case),
     };
 };

post '/checklist/add/*' => sub {
  redirect '/login' if !defined(session 'username');
  my ($case) = splat;
  if (params->{submit} eq "Save Progress") {
    update_checklist_result($case, (session 'username'), request->params);
  } elsif (params->{submit} eq "Submit and E-mail") {
    update_checklist_result_and_notify($case, (session 'username'), request->params);
  }

  loguser "Updated checklist in case# $case";
  flash('set',"Checklist updated!");

  redirect "/case/$case";
};

get '/jobsheet/*' => sub {
  redirect '/login' if !defined(session 'username');
  my ($case) = splat;
  set layout => 'print';
  my $entries = describe_sr($case);
  $entries->{description} =~ s/\r?\n/<br \/>/g;
  set layout => 'main';

  loguser "accessed jobsheet for $case";

  template 'jobsheet.tt',
    {
     pagename => "Jobsheet: " . $case,
     msg => flash('get'),
     blank => params->{blank},
     add_breaks => sub { $_[0] =~ s/\n/\<br \/\>/g; return $_[0]; },
     entries => describe_sr($case),
     add_time_uri => '/addtime',
     update_case_uri => '/update',
     users => sugar('list_accounts','id'),
     urgencies => get_urgencies(),
     this_case => $case,
     admins => get_admins(),
     activities_viewonly => 1,
     activities => get_case_activities($case),
     possibletimes => (possible_times()),
     tallytimes => tally_times_for_case($case),
     categories => get_categories(),
     statuses => get_statuses(session 'username'),
     timenow => &now(),
     lastdate => (session 'lastdate'  or &today()),
     lasttime => (session 'lasttime' or '12:00'),
     thisuser => session('username')
    };
};

get '/new/backup/*' => sub {
  redirect '/login' if !defined(session 'username');
  set layout => 'main';
  my ($hostname) = splat;
  my $details = &get_backup_failure($hostname);
  template 'new_case.tt',
    {
     pagename => "New Backup Failure",
     add_case_uri => '/add',
     users => sugar('list_accounts','name'),
     failure => $details,
     admins => get_admins(),
     urgencies => get_urgencies(),
     proposals => get_proposal_status(),
     categories => get_categories(),
     thisuser => (session 'username'),
    };
};

get '/new' => sub {
  redirect '/login' if !defined(session 'username');
  set layout => 'main';
  template 'new_case.tt',
    {
     pagename => "New Case",
     add_case_uri => '/add',
     users => sugar('list_accounts','name'),
     admins => get_admins(),
     urgencies => get_urgencies(),
     proposals => get_proposal_status(),
     categories => get_categories(),
     thisuser => session 'username',
    };
};

post '/add' => sub {
  redirect '/login' if !defined(session 'username');
  my $case = create_sr(undef, params->{category}, params->{customer},
                       params->{title}, 2, params->{tech}, params->{description},
		       params->{urgency}, params->{proposal}, params->{quote_no},
                       params->{contactname}, params->{phone}, params->{email},
                       (session 'username'));

  loguser "added case# $case";

  $case ? flash('set',"Case created: <a href=\"/case/$case\">#$case</a>") :
    flash('set',"Unable to create case");
  redirect '/';
};

post '/splitcase/*' => sub {
  redirect '/login' if !defined(session 'username');
  my ($case) = splat;
  my $newcase = split_case($case);
  $newcase ? flash('set', "Split activities into: <a href=\"/case/$newcase\">#$newcase</a>") :
    flash('set',"Unable to create case");
  redirect "/case/$case";
};

post '/merge_preview/*' => sub {
  redirect '/login' if !defined(session 'username');
  my ($fromcase) = splat;
  my $tocase = params->{to_case};
  if (!(describe_sr($tocase) && describe_sr($fromcase))) {
    flash('set',"$tocase does not exist");
    redirect "/case/$fromcase";
  }
  set layout => 'main';
  template 'merge_preview.tt',
    {
     pagename => "Merge Case",
     fromcase => $fromcase,
     tocase => $tocase,
     do_merge_uri => '/merge',
     name_lookup => sub { sugar('lookup_customer_name',$_[0]); },
     msg => flash('get'),
     entries => describe_sr($tocase),
     add_time_uri => '/addtime',
     update_case_uri => '/update',
     users => sugar('list_accounts','id'),
     this_case => $tocase,
     admins => get_admins(),
     activities => get_merge_case_activities($fromcase, $tocase),
     tallytimes => tally_times_for_merged_case($fromcase, $tocase),
     categories => get_categories(),
     statuses => get_statuses(session 'username'),
     timenow => &now(),
    };
};

post '/domerge' => sub {
  redirect '/login' if !defined(session 'username');
  my $fromcase = params->{fromcase};
  my $tocase = params->{tocase};
  if (merge_case($fromcase, $tocase, session 'username')) {
    flash('set',"$fromcase merged into $tocase and closed.");
  } else {
    flash('set',"Problem merging $fromcase into $tocase.");
  }

  loguser "applied a merge attaching $fromcase to $tocase";

  redirect "/case/$tocase"
};

post '/update' => sub {
  redirect '/login' if !defined(session 'username');
  my $case = params->{case};
  my $updated = update_case($case, params->{'customer'},
                            params->{'category'},
                            params->{'tech'},
                            params->{'status'}, params->{'title'},
                            params->{'description'}, params->{'parts'},
                            params->{'sfi'}, params->{'inv'}, params->{'urgency'},
                            params->{'proposal'}, params->{'order'},
                            params->{'contactname'},
                            params->{'phone'}, params->{'email'},
                            ((params->{'fixedrate'} =~ "N/A") ?
                             undef :
                             params->{'fixedrate'}),
                            params->{'quote_no'},
                            params->{'quote_value'},
                            params->{'quote_winner'},
                            (session 'username'));

  loguser "updated case $case (status: " . params->{'status'} . ")";

  $updated ? flash('set',"Case updated!") :
    flash('set',"Problem updating case.");
  redirect "/case/$case";
};

get '/list' => sub {
  redirect '/login' if !defined(session 'username');
  my ($res);
  my $mycases = 0;
  my ($title, $status, $customer, $tech, $category, $srno, $proposal, $global) =
    (
     params->{'s-title'}, params->{'s-status'},
     params->{'s-customer'}, params->{'s-tech'},
     params->{'s-category'}, params->{'s-srno'},
     params->{'s-proposal'}, params->{'s-global'}
    );
  session 'search-title' => $title;
  session 'search-status' => $status;
  session 'search-customer' => $customer;
  session 'search-tech' => $tech;
  session 'search-category' => $category;
  session 'search-proposal' => $proposal;
  session 'search-srno' => $srno;

  if (defined($srno) or defined($global) or defined(params->{'s-tech'})) {
    if ($srno) {
      redirect "/case/$srno";
    } elsif (defined($global) and $global) {
      if ($global ~~ /^#(?<srno>[0-9]+)/) {
        redirect "/case/" . $+{srno};
      } else {
        $res = quick_search(build_quick_search(undef, undef, undef, undef, undef, undef, 1, $global));
      }
    } else {
      $res = quick_search(build_quick_search($title, $status, $customer, $tech, $category, $proposal, 1));
    }
  } else {
    $mycases = 1;
    if (is_quote_master(session 'username')) {
      my ($search1, $search2, %result);
      $search1 = quick_search(build_quick_search('','2','ALL', 'ALL', 'Quote',''));
      $search2 = quick_search(build_quick_search('','2','ALL', (session 'username'), 'ALL',''));
      %result = (%$search1, %$search2);
      $res = \%result;
    } else {
      $res = quick_search(build_quick_search('','2','ALL', (session 'username'), 'ALL',''));
    }
  }

  my ($search, $count) = categorise_search(format_list($res), sugar('list_accounts','id'), $mycases);

  loguser "accessed the case list";

  set layout => 'main';
  template 'list.tt',
    {
     pagename => "Search",
     mobile => (session 'mobile_device'),
     mycases => $mycases,
     failures => &count_backup_failures,
     pftitle => session('search-title'),
     pfstatus => session('search-status'),
     pfcustomer => session('search-customer'),
     pftech => session('search-tech'),
     pfcategory => session('search-category'),
     pfproposal => session('search-proposal'),
     pfsrno => session('search-srno'),
     responsibility_lookup => sub { lookup_responsibility_name($_[0]); },
     admins => get_admins(),
     proposals => get_proposal_status(),
     categories => get_categories(),
     statuses => get_statuses(session 'username'),
     thisuser => (session('search-tech') or session('username')),
     users => sugar('list_accounts','name'),
     urgencies => get_urgencies(),
     msg => flash('get'),
     search => $search,
     count => $count,
    };
};

###
# Activities

get '/timesheet' => sub {
  set layout => 'main';
  my ($tech, $date) = (params->{tech}, params->{date});
  my $from = ($date . " 00:00");
  my $to = ($date . " 23:59");

  loguser "accessed $tech\'s timesheet for $date";

  if (params->{submit} eq "Create calendar") {
    template 'calendar.tt',
      {
       pagename => "Calendar: " . params->{tech},
       msg => '',
       tech => params->{tech},
       users => sugar('list_accounts','id'),
       admins => get_admins(),
       name_lookup => sub { sugar('lookup_customer_name',$_[0]); },
       activities => prepare_calendar_times(list_times_timesheet(params->{tech}, $from, $to)),
      };
  } else {
    template 'timesheet.tt',
      {
       pagename => "Timesheet: " . $tech,
       msg => '',
       tech => $tech,
       date => $date,
       admins => get_admins(),
       responsibility_lookup => sub { lookup_responsibility_name($_[0]); },
       name_lookup => sub { sugar('lookup_customer_name',$_[0]); },
       timesheet_pdf_uri => '/timesheet_pdf',
       times => list_times_timesheet($tech, $from, $to),
       oncall => list_oncall_timesheet($tech, $from, $to),
       tallyoncall => tally_oncall_timesheet($tech, $from, $to),
       notify_timesheet_uri => '/notify_timesheet',
      };
  }
};

post '/notify_timesheet' => sub {
  set layout => 'main';
  redirect '/login' if !defined(session 'username');
  if (!get_user_timesheet_for_date((session 'username'), params->{date})) {
    insert_timesheet_submit_time(params->{date}, (session 'username'));
  }
  loguser "sent " . params->{user} . "'s timesheet notification for " . params->{date};
  notify('timesheet', params->{user}, params->{date});
  flash('set',"Notification sent for " . params->{user} . "'s " . params->{date} . " timesheet");
  redirect '/';
};

get '/time/*' => sub {
  set layout => 'main';
  redirect '/login' if !defined(session 'username');
  my ($timeid) = splat;
  template 'time.tt',
    {
     pagename => "Edit Time",
     msg => '',
     escape => sub { encode_entities($_[0]) },
     time => get_time($timeid),
     update_time_uri => '/update_time',
     admins => get_admins(),
     billingcat => get_billing_categories(),
     thisuser => session 'username',
    };
};

get '/case_note/*' => sub {
  set layout => 'main';
  redirect '/login' if !defined(session 'username');
  my ($noteid) = splat;
  template 'case_note.tt',
    {
     pagename => "Edit Note",
     msg => '',
     escape => sub { encode_entities($_[0]) },
     notetypes => get_note_types(),
     case_note => get_case_note($noteid),
     update_note_uri => '/update_case_note',
    };
};

get '/remove_item_prompt/*' => sub {
set layout => 'main';
  redirect '/login' if !defined(session 'username');
  my ($itemid) = splat;
  my $type = params->{type};

  template 'remove_item.tt',
    {
     pagename => "Remove $type",
     type => $type,
     msg => flash('get'),
     item => (($type =~ 'note') ?
              get_case_note($itemid) :
              get_time($itemid)),
     remove_item_uri => (($type =~ 'note') ?
                         '/remove_case_note' :
                         '/remove_time'),
    };
};

post '/remove_time' => sub {
  set layout => 'main';
  redirect '/login' if !defined(session 'username');

  my $case = params->{case};

  loguser "removed time (ID: " . params->{id} . ") from $case";

  if (params->{cancel}) {
    redirect "/case/$case";
  } else {
    my $success = remove_time(params->{id});
    $success ? flash('set',"Time removed!") :
      flash('set',"Problem removing time.");
    redirect "/case/$case";
  }
};

post '/remove_case_note' => sub {
set layout => 'main';
  redirect '/login' if !defined(session 'username');
  my $case = params->{case};

  loguser "removed case note (ID: " . params->{id} . ") from $case";

  if (params->{cancel}) {
    redirect "/case/$case";
  } else {
    my $success = remove_case_note(params->{id});
    $success ? flash('set',"Note removed!") :
      flash('set',"Problem removing note.");
    redirect "/case/$case";
  }
};

post '/gensfi' => sub {
  redirect '/login' if !defined(session 'username');
  my $case = params->{this_case};
  if (generate_sfi($case, (session 'username'))) {
    loguser "generated SFI for $case";
    flash('set',"SFI generated successfully");
  } else {
    loguser "attempted to generate SFI for $case unsuccessfully";
    flash('set',"SFI could not be generated.  Try blanking the SFI, updating the case and retrying.");
  }
  redirect "/case/$case";
};

post '/update_time' => sub {
  redirect '/login' if !defined(session 'username');
  my $case = params->{orig_sr};
  my $fail = update_time(params->{id}, params->{sr},
                         params->{tech}, params->{from},
                         params->{to}, params->{billable},
                         params->{billcat}, params->{description});
  if (!$fail) {

    loguser "updated time " . params->{id} . " in case# " . params->{sr};

    flash('set',"Time updated!");
    session 'lastdate' => params->{date};
    session 'lasttime' => params->{to};
  } else {
    flash('set',"Problem updating time.");
  }
  redirect "/case/$case";
};

post '/update_case_note' => sub {
  redirect '/login' if !defined(session 'username');
  my $case = params->{orig_sr};
  my $success = update_case_note(params->{id}, params->{case},
                                 params->{visibility},
                                 params->{description});
  if ($success) {
    loguser "updated case note " . params->{id} . " in case# " . params->{case};
    flash('set',"Case note updated!");
  } else {
    flash('set',"Problem updating case note.");
  }
  redirect "/case/$case";
};

post '/addtime' => sub {
  redirect '/login' if !defined(session 'username');
  my $case = params->{case};
  my $success = create_time($case, params->{'tech'},
                            params->{'date'} . ' ' . params->{'from'},
                            params->{'date'} . ' ' . params->{'to'},
                            params->{'billable'}, params->{'description'});

  if ($success) {
    loguser "added times to $case";
    flash('set',"Time updated!");
    session 'lastdate' => params->{date};
    session 'lasttime' => params->{to};
  } else {
    flash('set',"Problem adding time.");
  }
  redirect "/case/$case";
};

post '/add_activity' => sub {
  redirect '/login' if !defined(session 'username');
  my ($success, $type);
  my $case = params->{case};
  my ($addtime, $addnote) = (params->{addtime}, params->{addnote});
  my $from = params->{'date'} . ' ' . params->{'from'};
  my $to = params->{'date'} . ' ' . params->{'to'};
  if (defined($addtime)) {
    $success = create_time($case, (session 'username'),
                           $from,
                           $to,
                           params->{'billable'}, params->{'billcat'},
                           params->{'description'});
    notify('customer_activity', $case, (session 'username'), params->{'description'},
           $from, $to, describe_sr($case), params->{'case-notify'}, params->{'auto-notify'});
    $type = "Time";
    session 'lastdate' => params->{date};
    session 'lasttime' => params->{to};
  } elsif (defined($addnote)) {
    my $upload = request->upload('attachment');
    $success = new_case_note($case,
                             (session ('username')),
                             params->{'visibility'},
                             $upload,
                             params->{'description'},
                             # Customer notifications
                             params->{'case-notify'},
                             params->{'auto-notify'}
                            );
    $type = "Note";
  } else {
    flash('set',"Something went wrong?!");
  }
  if ($success and $type) {
    loguser "added $type to $case";
    flash('set',"$type updated!");
  } else {
    flash('set',"Problem adding $type.");
  }
  redirect "/case/$case";
};

get '/make_stat' => sub {
  redirect '/login' if !defined(session 'username');

};

get '/statistics' => sub {
  redirect '/login' if !defined(session 'username');

  my $from = ((params->{from} ~~ /^(?<date>[0-9]{4}-[0-9]{2}-[0-9]{2})/x) and
              (params->{from} ~~ /[0-9]{2}:[0-9]{2}$/) ?
              params->{form} :
              ($+{date} . " 00:00"));

  my $to = ((params->{to} ~~ /^(?<date>[0-9]{4}-[0-9]{2}-[0-9]{2})/x) and
            (params->{to} ~~ /[0-9]{2}:[0-9]{2}$/) ?
            params->{to} :
            ($+{date} . " 23:59"));
  my ($times, $total) = group_times_by_tech($from, $to);

  loguser "generated statistics for " . params->{from};

  set layout => 'main';
  template 'stat.tt',
    {
     pagename => "Statistics",
     msg => '',
     start => params->{from},
     finish => params->{to},
     times => $times,
     invlat => invoice_latency($from, $to),
     ftime => sub { format_time($_[0]) },
     tio => tally_times_opened_cases(),
     total => $total,
    };
};

get '/calendar' => sub {
  redirect '/login' if !defined(session 'username');
  redirect '/' if defined(session 'portal');
  my $from = (params->{date} . " 00:00");
  my $to = (params->{date} . " 23:59");
  set layout => 'main';

  loguser "generated " . params->{tech} . "'s calendar for " . params->{date};


};

###
# Code

sub get_statuses {
  return query_db('fahr','get_statuses', 'value_key');
}

sub count_open {
  my $customer = shift;
  return query_db('fra', 'count_open', $customer);
}

sub get_categories {
  return query_db('faar','get_categories');
}

sub describe_sr {
  my $id = shift;
  return query_db('frhr','describe_sr', $id);
}

sub get_urgencies {
  return query_db('fahr','get_urgencies', 'value_key');
}

sub get_proposal_status {
  return query_db('fahr','get_proposal_status', 'value_key');
}

sub get_order_status {
  return query_db('fahr','get_order_status', 'value_key');
}

#TODO: Rename me
sub lookup_sr_no {
  my $syncid = shift;
  my ($id) = query_db('fra', 'lookup_sr_no', $syncid);
  return $id;
}

###
# Scoring
sub get_cases_for_meta {
  return query_db('fahr','get_cases_for_meta', 'id');
}

sub update_metadata_all_cases {
  my $cases = get_cases_for_meta();
  for (keys(%$cases)) {
    print "Updating metadata for $_...\n";
    &update_meta($_, score_this_case($cases->{$_}), $cases->{$_}->{duration},
                 $cases->{$_}->{billable});
  }
}

sub get_case_for_meta {
  my $case = shift;
  return query_db('frhr','get_case_for_meta', $case);
}

sub score_this_case {
  my $case = shift;
  my $score = 0;
  $score += score_time((28 * 24 * 60 * 60), $case->{createsec}) * 0.7;
  $score += score_time((28 * 24 * 60 * 60), $case->{updatesec}) * 0.3;
  $score += score_time((14 * 24 * 60 * 60), $case->{notesec}) * 0.5;
  $score += score_time((7 * 24 * 60 * 60), $case->{actsec});
  $score += score_urgency($case->{urgency}) * 5;
  $score += score_bh($case->{billable}) * 5;
  $score *= score_category($case->{problem_type});
  return $score;
}

sub score_category {
  my $cat = shift;
  if ($cat =~ /^(Internal|Leave)$/) {
    return 0.01;
  } else {
    return 0.5;
  }
}

sub score_bh {
  my $bh = shift;
  return 0 if !$bh;
  return $bh / 6;
}

sub score_urgency {
  my $urgency = shift;
  my @urgency_scores = (2.5, 0.85, 0.5, 0.1, 0.01);
  return $urgency_scores[$urgency - 1];
}

sub score_time {
  my ($tolerance, $sec) = @_;
  return 1 if (!defined($sec));
  return min(1, $sec / $tolerance);
}

sub update_meta_for_case {
  my $case = shift;
  my $data = get_case_for_meta($case);
  update_meta($case, score_this_case($data), $data->{duration},
              $data->{billable})
    if(defined($data));
}

sub update_meta {
  my ($case,$score, $duration, $billable) = @_;
  $duration = 0 if (!defined($duration));
  $billable = 0 if (!defined($billable));
  query_db('noret','update_meta_update',
                $score, $duration, ($billable * 60), $case);
  query_db('noret','update_meta_insert',
                $case, $score, $duration, ($billable * 60), $case);
}

#TODO Rename me??
sub calc_durations_call_cases {
  return query_db('fahr','get_cases_for_durations', 'id');
}

sub get_fixed_rates_for_customer {
  my $customer = shift;
  return query_db('fahr','get_fixed_rates_for_customer', 'id', $customer);
}

sub get_fixed_rate_details {
  my $id = shift;
  return 0 if(!defined($id));
  return query_db('frhr','get_fixed_rate_details', $id);
}

sub split_case {
  my $case_no = shift;
  my $details = describe_sr($case_no);
  my $new_case = create_sr(undef,
                           $details->{problem_type},
                           $details->{request_user},
                           "#$case_no Billing case - $details->{title}",
                           12,
                           $details->{responsibility},
                           "Billing from #$case_no",
                           2, 0, '', '', '', '', (session 'username'));
  new_case_note($case_no, (session 'username'), 1, undef,
                "Activities moved to: <a href=\"/case/$new_case\">#$new_case</a>");
  new_case_note($new_case, (session 'username'), 1, undef,
                "Activities moved from: <a href=\"/case/$case_no\">#$case_no</a>");
  split_out_case_activities($new_case, $case_no);
  update_meta_for_case($new_case);
  update_meta_for_case($case_no);
  return $new_case;
}

sub split_out_case_activities {
  my ($newcase, $oldcase) = @_;
  return query_db('noret','split_out_case_activities', $newcase, $oldcase);
}

sub generate_sfi {
  my $case = shift;
  my $user = shift;
  my $details = describe_sr($case);

  # Fail if SFI already present
  return 0 if $details->{resolution};

  my $info = query_db('frhr','case_summary', $case);
  my $sfi = "[BH: " . ($info->{billable} or '0') . " - " . $info->{today} . " " . $user . "]";
  my $res = query_db('fahr','build_sfi_info', 'fromid', $case);

  for my $row (sort(keys(%$res))) {
    $sfi = $sfi . "\n\n" . $res->{$row}->{detail};
  }
  $sfi =~ s/<br>//g;
  $sfi =~ s/BH: [0-9\.]+ \| //g;

  query_db('noret','update_gen_sfi', $sfi, $case);
  return 1;
}

sub merge_case {
  my ($fromcase, $tocase, $username) = @_;
  query_db('noret','move_times_to_new_sr', $tocase, $fromcase);
  query_db('noret','move_notes_to_new_sr', $tocase, $fromcase);
  query_db('noret','close_case_as_dup', $tocase, $fromcase);
  merge_case_details($tocase, $fromcase);
  new_case_note($tocase, $username, 1, undef, "Detached from <a href=\"$fromcase\">#$fromcase</a>");
  new_case_note($fromcase, $username, 1, undef, "Attached to <a href=\"$tocase\">#$tocase</a>");
  return 1;
}

sub merge_case_details {
  my ($to, $from) = @_;
  my $todetails = describe_sr($to);
  my $fromdetails = describe_sr($from);
  query_db('noret', 'merge_case_details',
           ($todetails->{description} . "\n\n----\nMerged case: " .
            $from .
            "\nTitle: " . $fromdetails->{title} . "\n" .
            $fromdetails->{description}),
           $to);
}

sub close_sr {
    my ($sr, $inv) = @_;
    if ($inv =~ /^[0-9]{5}$/) {
      query_db('noret','close_sr_with_inv', $inv, $sr);
      return 1;
    }
    return 0;
}

sub update_case {
  my ($case, $customer, $category, $tech, $status,
      $title, $description, $parts, $sfi, $inv, $urgency, $proposal,
      $order, $name, $phone, $email, $fixedrate, $quote_no, $quote_value,
      $quote_winner,
      $user) = @_;

  my $casedetails = describe_sr($case);
  my $origtech = $casedetails->{responsibility};
  my $origstatus = $casedetails->{status};

  query_db('noret','update_case', $customer, $category, $tech, $status, $title,
                $description, $parts, $urgency, ($proposal or undef), ($order or undef), $sfi,
                ($inv or undef), $name, $phone, $email, $fixedrate, $quote_no, ($quote_value or 0), $quote_winner, $case);

  # Resolved
  if ($status > 3 and $origstatus <= 3) {
    query_db('noret','set_close_time', $case);
    # Invoiced
  } elsif ($status == 19 and $origstatus != 19) {
    query_db('noret','set_invoice_time', $case);
  }

  # Accepted
  if ($casedetails->{proposal} != 10 and
     $proposal == 10) {
    query_db('noret','set_acceptance_time', $case);
  }

  update_meta_for_case($case);
  $customer = sugar('lookup_customer_name',$customer)
    unless ($customer =~ /New Customer/);

  if ($tech ne $origtech) {
    my $urgencies = get_urgencies;
    notify('caseupdate', $case, $urgency, $proposal, $tech, $title, $customer,
                      $name, $phone, "REASSIGNED", $description, $fixedrate,
                      $user, $urgencies);
    new_case_note($case, $user, 1, undef, "Job reassign from $origtech to $tech");
  }
  return 1;
}

sub create_sr {
  my ($syncid, $category, $customer, $title, $status, $tech,
      $description, $urgency, $proposal, $quote_no, $name, $phone, $email,
      $user) = @_;
  my $sr_no = lookup_sr_no($syncid);
  return $sr_no if defined($sr_no);
  query_db('noret','create_sr',
                $category,
                $title, $description, $status, $tech,
		$urgency, $proposal, $quote_no,
                &user_config('db','user'), &user_config('db','user'), $customer,
                $syncid, $name, $phone, $email);
  my $id = query_db('fra', 'find_new_sr', $category, $title, $description, $status, $tech,
               $customer);
  update_meta_for_case($id);
  $customer = sugar('lookup_customer_name',$customer)
    unless ($customer =~ /New Customer/);
  my $urgencies = get_urgencies;
  new_case_note($id, $user, 1, undef, "Job created assigned to $tech");
  notify('caseupdate', $id, $urgency, $proposal, $tech, $title, $customer, $name,
                    $phone, "CREATED", $description, undef, $user, $urgencies);
  notify('customer', $id, $title, "CREATED", $description, $user, $email)
    if $email;
  return $id;
}

sub list_times_for_case {
  my $case = shift;
  return query_db('fahr','list_times_for_case', 'timeid', $case);
}

sub get_case_activities {
  my $case = shift;
  return query_db('fahr','get_case_activities', 'timeid', $case);
}

sub get_merge_case_activities {
  my ($from_case, $to_case) = @_;
  return query_db('fahr','get_merge_case_activities', 'timeid', $from_case, $to_case);
}

sub list_times_for_merged_case {
  my ($from_case, $to_case) = @_;
  return query_db('fahr','list_times_for_merged_case', 'from_timef', $from_case, $to_case);
}

sub list_notes_for_merged_case {
  my ($from_case, $to_case) = @_;
  return query_db('fahr','list_notes_for_merged_case', 'time', $from_case, $to_case);
}

sub list_oncall_timesheet {
  my ($tech, $from, $to) = @_;
  return query_db('fahr','list_oncall_timesheet', 'from_timef',
                             $tech, $from, $to);
}

sub remove_time {
  my $id = shift;
  query_db('noret','remove_time', $id);
  update_meta_for_case($id);
  return 1;
}

sub get_time {
  my $id = shift;
  return query_db('frhr','time_details', $id);
}

sub get_admins {
  return query_db('fahr','get_admins', 'user_name');
}

sub get_organisations {
  return query_db('faar','get_organisations');
}

sub get_note_types {
  return query_db('fahr','get_note_types', 'value_key');
}

sub get_billing_categories {
  return query_db('fahr','get_billing_categories', 'value_key');
}

sub get_case_note_id {
  my $res = query_db('frhr', 'get_case_note_id', @_);
  return $res->{'id'} if $res;
}

sub new_case_note {
  my ($case, $user, $type, $attachment, $description, @cust_email) = @_;

  $attachment->filename =~ /\.(?<extn>[a-z]+?)$/i
    if $attachment;

  $description = $description . "\nUploaded File: " . $attachment->filename
    if $attachment;

  query_db('noret','new_case_note', $case, $user, $type,
           (defined($+{extn}) ? lc($+{extn}) : undef),
           $description);

  process_attachment($case, $user, $type, $attachment, $description)
    if $attachment;

  my $details = describe_sr($case);
  update_meta_for_case($case);
  notify('casenote', $case, $user, $description, $details);
  if ($type >= 2) {
    notify('customer_casenote', $case, $user, $description, $details, @cust_email);
  }
  return 1;
}

sub process_attachment {
  my ($case, $user, $type, $attachment, $description) = @_;
  $attachment->filename =~ /\.(?<extn>[a-z]+?)$/i;
  my $extn = lc($+{extn});

  my $note_id = get_case_note_id($case, $user, $type, $extn, $description);

  my $filename = $note_id . "-" . $attachment->filename;

  query_db('noret','case_note_add_filename', $filename, $note_id);

  $attachment->copy_to(&site_config('upload','notes_path') . "/" . $filename);


  if ($extn =~ /^(gif|jpg|png|bmp)$/) {
    my $image = Image::Imlib2->load(&site_config('upload','notes_path') . "/" .
                                    $filename);
    my $image2 = $image->create_scaled_image(180,0);
    $image2->save(&site_config('upload','notes_path') . "/" . $note_id .
                  '-thumb.' . $extn);
  }
  return $extn;
}

sub get_quote_name {
  my $quote_no = shift;
  return 0 if !$quote_no;
  my $quotes_path = &site_config('upload','quotes_path');

  opendir(DIR, $quotes_path) or die $!;
  my @quotes = grep
    {
      /^$quote_no([\-\s_].+)?\.pdf$/xi
    }
      readdir(DIR);
  return $quotes[0];
}

sub get_dtf_name {
  my $quote_no = shift;
  return 0 if !$quote_no;
  opendir(DIR, &site_config('quotewerks','dtf_path')) or return 0;
  my @quotes = grep
    {
      /^$quote_no([\-\s_].+)?\.dtf$/xi
    }
      readdir(DIR);
  return $quotes[0];
}

sub tally_times {
  my $row = shift;
  my %times;
  $times{normal_raw} = $row->{time};
  $times{billable_raw} += $row->{billable} if $row->{billable};
  for (('normal', 'billable')) {
    $times{$_} = sprintf("%d:%02d",
                         ($times{$_ . '_raw'} / 60),
                         ($times{$_ . '_raw'} % 60))
      if $times{$_ . '_raw'};
  }
  return \%times;
}

sub tally_times_for_case {
  my $case = shift;
  my %times;
  my $row = query_db('frhr','tally_times_for_case', $case);
  return tally_times($row);
}

sub tally_times_for_merged_case {
  my ($fromcase, $tocase) = @_;
  my %times;
  my $row = query_db('frhr','tally_times_for_merged_case', $fromcase, $tocase);
  return tally_times($row);
}

sub tally_times_timesheet {
  my ($tech, $from, $to) = @_;
  return tally_times(query_db('frhr','tally_times_timesheet',
                                   $tech, $from, $to));
}

sub tally_oncall_timesheet {
  my ($tech, $from, $to) = @_;
  return tally_times(query_db('frhr','tally_oncall_timesheet',
                                   $tech, $from, $to));
}

sub list_times_timesheet {
  my ($tech, $from, $to) = @_;
  my $res;
  my $dbres = query_db('fahr','list_times_timesheet', 'timeid', $tech, $from, $to);

  for my $time (keys(%$dbres)) {
    $time ~~ /(?<date>[0-9]{4}-[0-9]{2}-[0-9]{2}) / or die "Timestamp does not contain a date.";
    my $date = $+{date};
    $res->{$date}->{details}->{$time} = $dbres->{$time};

    $dbres->{$time}->{time_spent} =~ /(?<hours>[0-9]+):(?<mins>[0-9]+)/;
    $res->{$date}->{normal} += ($+{hours} * 60 + $+{mins});

    $dbres->{$time}->{billable} =~ /(?<hours>[0-9]+):(?<mins>[0-9]+)/;
    $res->{$date}->{billable} += ($+{hours} * 60 + $+{mins});
  }

  for my $time (keys(%$res)) {
    my $mins = $res->{$time}->{normal};
    $res->{$time}->{normal} = sprintf("%d:%02d", $mins / 60, $mins % 60);

    $mins = $res->{$time}->{billable};
    $res->{$time}->{billable} = sprintf("%d:%02d", $mins / 60, $mins % 60);
  }

  return $res;
}

sub create_time {
  my ($case, $tech, $from, $to, $billable, $billcat,
      $description) = @_;
  my $orig_times = tally_times_for_case($case);
  my $orig_hours = (defined($orig_times->{billable}) ? (time_to_sec($orig_times->{billable}) / 3600) : 0);
  $billable = "0.0" if (!looks_like_number($billable) or $billable < 0);

  query_db('noret','create_time',
                $case, $tech, $from, $to, $description, $billable, $billcat);
  update_meta_for_case($case);

  my $new_times = tally_times_for_case($case);
  my $new_hours= (defined($new_times->{billable}) ?
                  (time_to_sec($new_times->{billable}) / 3600) : 0);
  my $case_details = describe_sr($case);

  notify('milestone', $case, $new_times, $case_details)
    if ((int($new_hours / 10) > int($orig_hours / 10)) or
        (defined($case_details->{fixed_rate}) and
         $new_hours > $case_details->{fixed_rate}
         and
         $orig_hours <= $case_details->{fixed_rate}));
  return 1;
}

sub lookup_responsibility_name {
  my $full_name = shift;
  chomp $full_name;
  return ($full_name or "Unassigned");
}

sub update_time {
  my ($id, $case, $tech, $from, $to, $billable, $billcat, $description) = @_;
  query_db('noret','update_time', $case, $tech, $from, $to, $billable,
                $billcat, $description, $id);
  update_meta_for_case($id);
  return 0;
}

sub get_times_for_date_range {
  my ($from, $to) = @_;
  return query_db('fahr','get_times_for_date_range', 'wrid', $from, $to);
}

sub get_case_notes {
  my ($case,$restriction) = @_;
  return query_db('fahr','get_case_notes', 'id', $case, $restriction);
}

sub get_case_note {
  my ($noteid) = @_;
  return query_db('frhr','get_case_note', $noteid);
}

sub remove_case_note {
  my $id = shift;
  query_db('noret','remove_case_note', $id);
  update_meta_for_case($id);
  return 1;
}

sub update_case_note {
  my ($id, $case, $type, $description) = @_;
  query_db('noret','update_case_note', $case, $type, $description, $id);
  update_meta_for_case($id);
  return 1;
}

sub invoice_latency {
  my ($from, $to) = @_;
  return query_db('frhr','invoice_latency', $from, $to);
}

sub tally_times_opened_cases {
  return query_db('frhr','tally_times_opened_cases');
}

sub get_sales_for_date_range {
  my ($from, $to) = @_;
  return query_db('fahr','get_sales_for_date_range', 'quote_winner', $from, $to);
}

#TODO: FIXME
sub group_times_by_tech {
  my ($from, $to) = @_;
  my $raw = &get_times_for_date_range($from, $to);
  my $sales = &get_sales_for_date_range($from, $to);
  my $timesheets = &timesheet_submission_statistics($from, $to);
  my $opened = &stat_cases_opened_in_date_range($from, $to);
  my $closed = &stat_cases_closed_in_date_range($from, $to);
  my $taken = &stat_cases_taken_by_technician($from, $to);
  my $wikiedits = 0;

  my ($res, $total, $tech);

  for my $time (keys(%$raw)) {
    $tech = $raw->{$time}->{tech};
    $res->{$tech}->{techname} = $raw->{$time}->{techname};
    $res->{$tech}->{billable} += $raw->{$time}->{billable} if $raw->{$time}->{billable};

    $res->{$tech}->{billed} += $raw->{$time}->{billable} if $raw->{$time}->{billable} and
      $raw->{$time}->{status} >= &user_config('db','case_closed_status');

    $res->{$tech}->{reviewed} += $raw->{$time}->{billable} if $raw->{$time}->{billable} and
      $raw->{$time}->{status} >= &user_config('db','case_reviewed_status');

    $res->{$tech}->{invoiced} += $raw->{$time}->{billable} if $raw->{$time}->{billable} and
      $raw->{$time}->{status} == &user_config('db','case_billed_status');

    $res->{$tech}->{normal} += $raw->{$time}->{normal} if $raw->{$time}->{normal};

    $total->{normal} += $raw->{$time}->{normal} if $raw->{$time}->{normal};
    $total->{billable} += $raw->{$time}->{billable} if $raw->{$time}->{billable};
    $total->{billed} += $raw->{$time}->{billable} if $raw->{$time}->{billable} and
      $raw->{$time}->{status} >= &user_config('db','case_closed_status');

    $total->{reviewed} += $raw->{$time}->{billable} if $raw->{$time}->{billable} and
      $raw->{$time}->{status} >= &user_config('db','case_reviewed_status');

    $total->{invoiced} += $raw->{$time}->{billable} if $raw->{$time}->{billable} and
      $raw->{$time}->{status} == &user_config('db','case_billed_status');
  }

  for (keys(%$res)) {
    $res->{$_}->{sales_value} = $sales->{$_}->{sales_value};
    $total->{sales_value} += $sales->{$_}->{sales_value}
      if defined($sales->{$_}->{sales_value});
    $total->{wikiedits} += $wikiedits->{$_}->{edits}
      if defined($wikiedits->{$_}->{edits});

    $res->{$_}->{timesheets} = sprintf("%.1f",
                                       (defined($timesheets->{$_}->{result})) ?
                                       $timesheets->{$_}->{result} : 0);
    $res->{$_}->{wikiedits} = $wikiedits->{$_}->{edits}
      if defined($wikiedits->{$_}->{edits});
    $total->{timesheet}->{hits} += $timesheets->{$_}->{hit}
      if defined($timesheets->{$_}->{hit});
    $total->{timesheet}->{total} += $timesheets->{$_}->{total}
      if defined($timesheets->{$_}->{total});

    $res->{$_}->{cases}->{taken} = $taken->{$_}->{count};
    $res->{$_}->{cases}->{closed} = $closed->{$_}->{count};

    $res->{$_}->{cases}->{ratio} = sprintf("%.2f",
                                           (defined($res->{$_}->{cases}->{taken}) and
                                            defined($res->{$_}->{cases}->{closed}) and
                                            $res->{$_}->{cases}->{closed}) ?
                                           $res->{$_}->{cases}->{taken} / $res->{$_}->{cases}->{closed}
                                           : 0);

    $total->{cases}->{opened} += $opened->{$_}->{count}
      if defined($opened->{$_}->{count});
    $total->{cases}->{closed} += $closed->{$_}->{count}
      if defined($closed->{$_}->{count});

    $res->{$_}->{perc_bh} = sprintf("%.1f", ($res->{$_}->{normal} && $res->{$_}->{billable} ?
                                             ($res->{$_}->{billable} / $res->{$_}->{normal} * 100)
                                             : 0));

    $res->{$_}->{perc_closed} = sprintf("%.1f", ($res->{$_}->{billed} && $res->{$_}->{billable} ?
                                                 ($res->{$_}->{billed} / $res->{$_}->{billable} * 100)
                                                 : 0));

    $res->{$_}->{perc_revd} = sprintf("%.1f", ($res->{$_}->{reviewed} && $res->{$_}->{billed} ?
                                               ($res->{$_}->{reviewed} / $res->{$_}->{billed} * 100)
                                               : 0));

    $res->{$_}->{perc_invd} = sprintf("%.1f", ($res->{$_}->{invoiced} && $res->{$_}->{reviewed} ?
                                               ($res->{$_}->{invoiced} / $res->{$_}->{reviewed} * 100)
                                               : 0));

    $res->{$_}->{normal} = format_time($res->{$_}->{normal});
    $res->{$_}->{billable} = format_time($res->{$_}->{billable});
    $res->{$_}->{billed} = format_time($res->{$_}->{billed});
    $res->{$_}->{reviewed} = format_time($res->{$_}->{reviewed});
    $res->{$_}->{invoiced} = format_time($res->{$_}->{invoiced});
  }

  $total->{perc_bh} = sprintf("%.1f", ($total->{normal} && $total->{billable} ?
                                           ($total->{billable} / $total->{normal} * 100)
                                           : 0));

  $total->{perc_closed} = sprintf("%.1f", ($total->{billed} && $total->{billable} ?
                                               ($total->{billed} / $total->{billable} * 100)
                                               : 0));

  $total->{perc_revd} = sprintf("%.1f", ($total->{reviewed} && $total->{billed} ?
                                         ($total->{reviewed} / $total->{billed} * 100)
                                               : 0));

  $total->{perc_invd} = sprintf("%.1f", ($total->{invoiced} && $total->{reviewed} ?
                                             ($total->{invoiced} / $total->{reviewed} * 100)
                                             : 0));
  $total->{normal} = format_time($total->{normal});
  $total->{billable} = format_time($total->{billable});
  $total->{billed} = format_time($total->{billed});
  $total->{reviewed} = format_time($total->{reviewed});
  $total->{invoiced} = format_time($total->{invoiced});
  $total->{timesheet}->{ratio} = sprintf("%.2f", (defined($total->{timesheet}->{hits}) and
                                                  defined($total->{timesheet}->{total}) and
                                                  $total->{timesheet}->{total} ?
                                                  $total->{timesheet}->{hits} /
                                                  $total->{timesheet}->{total}
                                                  * 100
                                                  : 0));

  $total->{cases}->{ratio} = sprintf("%.2f", (defined($total->{cases}->{opened}) and
                                                  defined($total->{cases}->{closed}) and
                                                  $total->{cases}->{closed} ?
                                                  $total->{cases}->{opened} /
                                                  $total->{cases}->{closed}
                                                  : 0));

  return ($res, $total);
}

sub build_quick_search {
  my ($title, $status, $customer, $tech, $category,$proposal,$search, $global) = @_;

  my $query = "SELECT sr.id, (cast(round(cast(qs.score as numeric),10) as text) || cast(sr.id as text)) as sortpos, order_status, sr.cust_text2 AS syncid, sr.cust_int2 as proposal, title, urgency, request_user, responsibility, (u.first_name || ' ' || u.last_name) as full_name, status, problem_type, sr.cust_int1, due_date, billable, duration, DATE(insert_time) as logged, wr.cust_list1 as b, to_char(ut.update_time, 'YYYY-MM-DD') as last_update_time FROM service_req as sr LEFT JOIN work_report as wr ON sr.id = wr.service_req_id LEFT JOIN users as u ON u.user_name = sr.responsibility LEFT JOIN meta as qs ON sr.id = qs.id LEFT JOIN notes as n ON sr.id = n.sr_id LEFT JOIN (SELECT q.srid,max(q.time) as update_time from (SELECT service_req_id as srid,max(to_time) as time from work_report group by srid UNION SELECT sr_id as srid,max(timestamp) as time from notes group by srid UNION SELECT id as srid, update_time as time from service_req) as q group by q.srid) as ut ON sr.id = ut.srid where " . (defined($search) ? "(" : "((responsibility = 'none' AND status<=3) OR ") . " (responsibility <> '' ";
  my (@conds);

  if ($global) {
    for (qw/title sr.description problem_type resolution sr.cust_notes sr.full_name sr.contact contact_email
            cust_int1 quote_no wr.description n.description/) {
      push(@conds, ("cast($_ as text) ILIKE '%$global'")) and next
        if ($_ =~ /^cust_int1$/);
      push(@conds, ("$_ ILIKE '%$global%'"));
    }
    my $cust_search = sugar('search_accounts',$global);
    for (@{$cust_search}) {
      push(@conds, ("request_user = '" . $_->[0] . "'"));
    }
    $query = $query . " AND (" . join(" OR ", @conds) . ")" if @conds;
  } else {
    push(@conds, ("title ILIKE '%" . $title . "%'")) if $title;
    push(@conds, ("status <= 3")) if ($status and $status == 2);
    push(@conds, ("status = " . $status)) if ($status and $status > 3 and $status ne 'ALL');
    push(@conds, ("request_user  = '" . $customer . "'")) if ($customer && $customer ne 'ALL');
    push(@conds, ("responsibility = '" . $tech . "'")) if ($tech && $tech ne 'ALL');
    push(@conds, ("problem_type = '" . $category . "'")) if ($category && $category ne 'ALL');
    push(@conds, ("sr.cust_int2 = '" . $proposal . "'")) if ($proposal && $proposal ne 'ALL');
    $query = $query . " AND " . join(" AND ", @conds) if @conds;
  }
  return $query . ")) GROUP BY sr.id, qs.score, sr.cust_text2, title, urgency, request_user, responsibility, status, problem_type, sr.cust_int1, due_date, billable, duration, sr.cust_int2, logged, wr.cust_list1, u.first_name, u.last_name, ut.update_time, order_status;";
}

sub calendar_times {
  my @times = ();
  for (8 .. 17) {
    push @times, "$_:00";
    push @times, "$_:15";
    push @times, "$_:30";
    push @times, "$_:45";
  }
  return @times;
}

sub prepare_calendar_times {
  my $res = shift;
  my ($cal, $status);
  my @dates;
  $cal->{count} = 0;
  for my $time (calendar_times()) {
    for my $date (keys(%$res)) {
      $status = calendar_assign_status($res->{$date}->{details}, $time);
      $cal->{times}->{time_to_sec($time)}->{blocks}->{$date} = $status;
      $cal->{count}+=1 if ($status ne "none");
      push(@dates, $date) if (!grep(/$date/, @dates));
    }
    $cal->{times}->{time_to_sec($time)}->{time} = $time;
  }
  $cal->{dates} = \@dates;
  return $cal;
}

sub calendar_assign_status {
    my ($activities, $time) = @_;
    my $time_raw = time_to_sec($time);
    for my $activity (sort(keys(%$activities))) {
      if ($activities->{$activity}->{from_timef_timeonly} <= $time_raw &&
	  $activities->{$activity}->{to_timef_timeonly} > $time_raw) {
	if ($activities->{$activity}->{problem_type} eq "Leave") {
	  return {
		  status => "leave",
		  sr => $activities->{$activity}->{sr},
		  customer => $activities->{$activity}->{request_user},
		  title => $activities->{$activity}->{title},
		  };
	}
	elsif ($activities->{$activity}->{billable} ne "00:00") {
	  return {
		  status => "billable",
		  sr => $activities->{$activity}->{sr},
		  customer => $activities->{$activity}->{request_user},
		  title => $activities->{$activity}->{title},
		  };
	  return ["billable", $activities->{$activity}->{sr}];
	}
	else {
	  return {
		  status => "normal",
		  sr => $activities->{$activity}->{sr},
		  customer => $activities->{$activity}->{request_user},
		  title => $activities->{$activity}->{title},
		  };
	}
      }
    }
    return {
	    status => "none",
	   };
}

sub categorise_search {
  my ($res, $accounts, $mycases) = @_;
  my $catd;
  my $count = 0;
  $catd->{00}->{meta}->{title} = "unassigned";
  $catd->{10}->{meta}->{title} = "overdue";
  $catd->{20}->{meta}->{title} = "due";
  $catd->{30}->{meta}->{title} = "upcoming";
  $catd->{40}->{meta}->{title} = "pool";
  $catd->{51}->{meta}->{title} = "quotes - to do";
  $catd->{53}->{meta}->{title} = "quotes - waiting on info";
  $catd->{55}->{meta}->{title} = "quotes - sent to client";
  $catd->{56}->{meta}->{title} = "quotes - closed";
  $catd->{57}->{meta}->{title} = "to be ordered";
  $catd->{59}->{meta}->{title} = "order placed";
  $catd->{60}->{meta}->{title} = "site maintenance";
  $catd->{70}->{meta}->{title} = "on hold";
  $catd->{80}->{meta}->{title} = "placeholders";
  $catd->{95}->{meta}->{title} = "resolved";

  my $placeholder_cat = &user_config('category','placeholder');
  my $sitemaintenance_cat = &user_config('category','sitemaintenance');
  for my $key (sort(keys(%$res))) {
    $res->{$key}->{customer_name} = ($res->{$key}->{request_user} =~ "New Customer" ? "New Customer" :
                                     $accounts->{$res->{$key}->{request_user}}->{name});
    if (defined($res->{$key}->{status}) and $res->{$key}->{status} > 3) {
      $catd->{95}->{data}->{$key} = $res->{$key};
    } elsif (defined($res->{$key}->{status}) and $res->{$key}->{status} == 3) {
      $catd->{70}->{data}->{$key} = $res->{$key};
    } elsif (defined($res->{$key}->{responsibility}) and $res->{$key}->{responsibility} eq 'none' and $mycases) {
      $catd->{00}->{data}->{$key} = $res->{$key};
    } elsif (defined($res->{$key}->{'problem_type'}) and
             $res->{$key}->{'problem_type'} ~~ &user_config('category','quote')) {
      if ($res->{$key}->{proposal} =~ &user_config('category','quote_sent')) {
        $catd->{55}->{data}->{$key} = $res->{$key};
      } elsif ($res->{$key}->{proposal} =~
               &user_config('category','quote_closed')) {
        $catd->{56}->{data}->{$key} = $res->{$key};
      } elsif ($res->{$key}->{proposal} =~
               &user_config('category','quote_waiting')) {
        $catd->{53}->{data}->{$key} = $res->{$key};
      } else {
        $catd->{51}->{data}->{$key} = $res->{$key};
      }
    } elsif (defined($res->{$key}->{'problem_type'}) and
             $res->{$key}->{'problem_type'} ~~ &user_config('category','order')) {
      if ($res->{$key}->{order_status} ~~
          &user_config('category','order_placed')) {
        $catd->{59}->{data}->{$key} = $res->{$key};
      } else {
        $catd->{57}->{data}->{$key} = $res->{$key};
      }
    } elsif ($res->{$key}->{due_date} and $res->{$key}->{due_date} lt strftime("%Y-%m-%d", localtime)) {
      $catd->{10}->{data}->{$key} = $res->{$key};
    } elsif ($res->{$key}->{due_date} and $res->{$key}->{due_date} eq strftime("%Y-%m-%d", localtime)) {
      $catd->{20}->{data}->{$key} = $res->{$key};
    } elsif ($res->{$key}->{due_date} and
             $res->{$key}->{due_date} lt strftime("%Y-%m-%d", localtime(time + 86400 * 7))) {
      $catd->{30}->{data}->{$key} = $res->{$key};
    } elsif (defined($res->{$key}->{'problem_type'}) and 
             $res->{$key}->{'problem_type'} ~~ /$placeholder_cat/) {
      $catd->{80}->{data}->{$key} = $res->{$key};
    } elsif (defined($res->{$key}->{'problem_type'}) and
             $res->{$key}->{'problem_type'} ~~ /$sitemaintenance_cat/) {
      $catd->{60}->{data}->{$key} = $res->{$key};
    } else {
      $catd->{40}->{data}->{$key} = $res->{$key};
    }
    $count += 1;
  }
  return ($catd, $count);
}

sub format_time {
  my $secs = shift;
  my $time = "0:00";
  if ($secs) {
    $time = sprintf("%d:%02d", ($secs / 60.0) / 60.0, ($secs / 60.0) % 60.0);
  }
  return $time;
}

sub time_to_sec {
  my $timef = shift;
  $timef ~~ /(?<hours>[0-9]+):(?<mins>[0-9]{2})/ or die "Input is not a time";
  return $+{hours} * 60 * 60 + $+{mins} * 60;
}

sub format_list {
  my $data = shift;
  for my $key (sort(keys(%$data))) {
    $data->{$key}->{score} = 0
      if (!defined($data->{$key}->{score}) ||
          !looks_like_number($data->{$key}->{score}));
    if (defined($data->{$key}->{billable}) &&
	$data->{$key}->{billable} =~ /[0-9.]+/) {
      $data->{$key}->{billable} = format_time($data->{$key}->{billable} * 60);
    } else {
      $data->{$key}->{billable} = "0:00";
    }
    if (defined($data->{$key}->{duration}) &&
	$data->{$key}->{duration} =~ /[0-9.]+/) {
      $data->{$key}->{duration} = format_time($data->{$key}->{duration} * 60);
    } else {
      $data->{$key}->{duration} = "0:00";
    }
  }
  return $data;
}

sub possible_times {
  my @times = ();
  for (0 .. 23) {
    push @times, "$_:00";
    push @times, "$_:15";
    push @times, "$_:30";
    push @times, "$_:45";
  }
  push @times, "23:59";
  return \@times;
}

sub get_checklist_results {
  my ($case) = @_;
  return query_db('fahr', 'get_checklist_results', 'check_type_id', $case);
}

sub get_check_options {
  return query_db('fahr', 'get_check_options', 'id');
}

sub get_checklist_checks {
  return query_db('fahr', 'get_checklist_checks', 'id');
}

sub update_checklist_result {
  my ($case, $user, %params) = @_;

  my ($check_type, $detail, $checks, $message, %report);
  my $failure = 0;
  for (keys(%params)) {
    if ($_ =~ /^check-/) {
      ($check_type = $_) =~ s/check-//
        or die "Could not determine checktype";
      query_db('noret', 'update_checklist_result_insert',
               $case, $check_type, $params{$_}, $user, $case, $check_type);
      query_db('noret', 'update_checklist_result_update',
               $params{$_}, $user, $check_type, $case);

      $checks = get_checklist_checks();
      $report{$params{$_}} .= "\t- " . ($checks->{$check_type}->{description} . "\n");
    }
  }
  return \%report;
}

sub update_checklist_result_and_notify {
  my ($case, $user, %params) = @_;
  my $report = update_checklist_result($case, $user, %params);

  my $detail = describe_sr($case);

  # Follow up needed!
  if (defined($report->{2}) and $report->{2}) {
    create_sr(undef, $detail->{problem_type}, $detail->{request_user},
              "Site Maintenance checks needing follow-up",
              2, $user, ("Follow-up needed on failed checks: \n" .
                         $report->{2}),
              4, 1, '', '', '', '', $user);
  }

  &notify('smcheck', $case, $user, (session 'full_name'),
          sugar('lookup_customer_name',$detail->{request_user}), &today, $report);
  new_case_note($case, $user, 1, undef, "Checklist submitted");
}

sub create_checklist_form {
  return query_db('fahr', 'create_checklist_form', 'position');
}

sub today {
  return strftime("%Y-%m-%d", localtime);
}

sub now {
  return strftime('%Y-%m-%d %H:%M', localtime);
}

# User / customer related
sub valid_customer {
    my $customer = shift;
    return $customer ~~ (get_users());
}

sub is_admin {
  my $user = shift;
  my $admins = &user_config('db', 'admins');
  return ($user ~~ @{$admins});
}

sub is_quote_master {
  my $user = shift;
  my $qmaster = &user_config('default_view','quote_masters');
  return ($user ~~ @{$qmaster});
}

sub count_backup_failures {
  return query_db('frhr', 'count_backup_failures');
}

sub get_backup_failure {
  my $hostname = shift;
  return query_db('frhr', 'get_backup_failure', $hostname);
}

sub get_user_timesheet_for_date {
  my ($user, $date) = @_;
  return query_db('frhr', 'get_user_timesheet_for_date', $user, $date);
}

sub insert_timesheet_submit_time {
  my ($timesheet_date, $username) = @_;
  return query_db('noret', 'insert_timesheet_submit_time',
                  $timesheet_date, $username);
}

sub timesheet_submission_statistics {
  my ($from, $to) = @_;
  my $report = query_db('fahr', 'timesheet_submission_statistics', 'id',
                        $from, $to);
  my %res;
  for (keys(%$report)) {
    $res{$report->{$_}->{user_name}}{hit} += 0.5
      if ($report->{$_}->{sameday});
    $res{$report->{$_}->{user_name}}{hit} += 0.5
      if ($report->{$_}->{ontime});
    $res{$report->{$_}->{user_name}}{total} += 1;
  }
  for (keys(%res)) {
    $res{$_}{result} = ($res{$_}{hit} / $res{$_}{total} * 100)
      if (defined($res{$_}{hit}) and defined($res{$_}{total}));
  }
  return \%res;
}

sub stat_cases_opened_in_date_range {
  my ($from, $to) = @_;
  return query_db('fahr', 'stat_cases_opened_in_date_range',
                  'responsibility', $from, $to);
}

sub stat_cases_closed_in_date_range {
  my ($from, $to) = @_;
  return query_db('fahr', 'stat_cases_closed_in_date_range',
                  'responsibility', $from, $to);
}

sub stat_cases_taken_by_technician {
  my ($from, $to) = @_;
  return query_db('fahr', 'stat_cases_taken_by_technician',
                  'responsibility', $from, $to);
}

1;

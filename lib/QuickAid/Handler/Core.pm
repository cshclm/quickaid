package QuickAid::Handler::Core;

use Dancer ':syntax';
use Dancer::Plugin::MobileDevice;

use Dancer::Timer;
use QuickAid::Security;
use QuickAid::DB qw/query_db/;
use QuickAid::Config::UserConfig qw/user_config/;
use QuickAid::Log qw/ loginfo /;
use QuickAid::Log qw/ loguser /;

hook before => sub {
  my $timer = Dancer::Timer->new();
  var timer => $timer;
  var elapsed => \&get_page_load_time;
  session 'embedded' => 1 if defined(params->{embedded});
  if (!defined(session('username')) && request->path_info !~ m{^/(do)?login}) {
    var requested_path => request->request_uri;
    request->path_info('/login');
  }
};

hook before_layout_render => sub {
  my $timer = vars->{timer};
};

get '/login' => sub {
  set layout => 'basic';
  # Disable entry to this while QA under heavy development
  # if (is_mobile_device) {
  #  session 'embedded' => 'y';
  #  session 'mobile' => 'y';
  #}

  template 'login.tt',
    {
     msg => flash_get(),
     pagename => "Login",
     path => vars->{requested_path},
     login_url => '/dologin',
     mobile => is_mobile_device,
    };
};

post '/dologin' => sub {
  set layout => 'main';
  my $username = params->{username};
  my $login_attempts = get_login_attempts($username);
  if (defined($login_attempts) and !$login_attempts->{lockout}) {
    &failed_login_attempt_reset($username);
  }
  if (defined($login_attempts) and
      $login_attempts->{failed_logins} >= &user_config('security','login_attempts') and
      $login_attempts->{lockout}) {
    flash_set('Too many failed attempts.  Account locked for the next 5mins.');
    &failed_login_attempt(params->{username});
    redirect '/login';
  } elsif (&validate_login($username, params->{password})) {
    &failed_login_attempt_reset($username);
    if (params->{path}) {
      redirect (params->{path});
    } else {
      redirect '/';
    }
  } else {
    &failed_login_attempt(params->{username});
    flash_set('Wrong username or password');
    redirect '/login';
  }
};

get '/logout' => sub {
  set layout => 'main';
  my $embed = 0;
  if (session('embedded')) {
    $embed = 1;
  }

  loguser "logged out";

  session->destroy;
  if ($embed) {
    session 'embedded' => 'y';
  }
  flash_set('You are logged out');
  redirect '/login';
};

sub flash_set {
  session 'visits' => shift;
}

sub flash_get {
  my $msg = session 'visits' if defined(session 'visits');
  session 'visits' => undef;
  return $msg;
}

sub get_page_load_time {
  my $time = (vars->{timer})->tick();
  loginfo (request->request_uri . ": Page load time: $time");
  return $time . 's';
}

sub failed_login_attempt {
  my $user = shift;
  return query_db('noret', 'failed_login_attempt', $user);
}

sub get_login_attempts {
  my $user = shift;
  return query_db('frhr', 'get_login_attempts', $user);
}

sub failed_login_attempt_reset {
  my $user = shift;
  return query_db('noret', 'failed_login_attempt_reset', $user);
}

1;

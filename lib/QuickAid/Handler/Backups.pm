package QuickAid::Handler::Backups;

use QuickAid::Log qw/ loguser logdebug /;
use QuickAid::DB qw/ query_db quick_search /;

use Dancer ':syntax';

get '/backups' => sub {
  set layout => 'main';
  template 'backups.tt',
    {
     pagename => "Backups",
     msg => '',
     backups => get_backup_problems(),
     imagemanager => get_imagemanager_problems(),
    };
};

get '/backups/resolve/*' => sub {
  set layout => 'main';
  redirect '/login' if !defined(session 'username');
  my ($hostname) = splat;
  resolve_backup_problems($hostname);
  redirect '/backups';
};

get '/backups/im/resolve/*' => sub {
  set layout => 'main';
  redirect '/login' if !defined(session 'username');
  my ($email) = splat;
  resolve_imagemanager_problems($email);
  redirect '/backups';
};

#TODO
#post '/backups/resolve' => sub {
#};

sub get_backup_problems {
  return query_db('fahr', 'get_backup_problems', 'hostname');
}

sub get_imagemanager_problems {
  return query_db('fahr', 'get_imagemanager_problems', 'email');
}

sub resolve_backup_problems {
  my $hostname = shift;
  return query_db('noret', 'resolve_backup_problems',
                 $hostname, "Resolved by " . (session 'username'));
}

sub resolve_imagemanager_problems {
  my $email = shift;
  return query_db('noret', 'resolve_imagemanager_problems',
                 $email, "Resolved by " . (session 'username'));
}

1;

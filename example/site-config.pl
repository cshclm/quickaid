use vars qw($site_config);

$site_config =
  {
   site => {
            adhost => 'my.ldap.server',
            domain => 'ldap.domain',
            uri_base => 'https://my.qa.host',
           },

   psql => {
           type => 'Pg',
           name => 'quickaid',
           host => 'my.qa.host',
           port => '5432',
           user => 'quickaid',
           passwd => 'mypassword'
          },
   sugar => {
           type => 'mysql',
           name => 'sugarcrm',
           host => 'my.sugar.host',
           port => '3306',
           user => 'sugarcrm',
           passwd => 'mysugarpassword'
          },
  };

1;
